
#ifndef TERMINAL_H
#define TERMINAL_H

#include <stdint.h>

void print_tb(const char *str, int x, int y, uint16_t fg, uint16_t bg);
void printf_tb(int x, int y, uint16_t fg, uint16_t bg, const char *fmt, ...);

#endif
