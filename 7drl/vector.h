#ifndef VECTOR_H
#define VECTOR_H

#include <stddef.h>

#define VECTOR_ERROR_NOMEM (-1)

struct vector {
	/* Allocated space in bytes. */
	size_t allocd;
	/* Current length of vector. */
	size_t length;
	/* Size of each element in bytes. */
	size_t elem_size;
	/* Aligned raw byte array. Can only be free'd internally. */
	void *data;
	/* Internal buffer for sorting. */
	void *_buffer;
};

int vector_init(struct vector *v, size_t initial, size_t elem_size);
void vector_uninit(struct vector *v);
int vector_shrink(struct vector *v);
int vector_reserve(struct vector *v, size_t size);
int vector_copy(struct vector *v, struct vector *out);
int vector_push_back(struct vector *v, void *data);
int vector_push_front(struct vector *v, void *data);
void vector_pop_back(struct vector *v, void *out);
void vector_pop_front(struct vector *v, void *out);
int vector_ordered_insert(struct vector *v, void *data, int (*cmp)(const void *, const void *));
void vector_sort(struct vector *v, int (*cmp)(const void *, const void *));
int vector_bsearch(struct vector *v, void *data, int (*cmp)(const void *, const void *), void *out);
void vector_get(struct vector *v, size_t idx, void *out);
void vector_ref(struct vector *v, size_t idx, void **ref);
void vector_set(struct vector *v, size_t idx, void *data);
void vector_del(struct vector *v, size_t idx, void *out);

#endif
