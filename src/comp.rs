
use std::fmt;

use specs::*;

use util::*;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
pub struct Position {
    pub x: i32,
    pub y: i32
}
impl Position {
    pub fn new(x: i32, y: i32) -> Position {
        Position { x, y }
    }
}
impl Component for Position {
    type Storage = VecStorage<Self>;
}
impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}, {}", self.x, self.y)
    }
}
impl From<Position> for Direction {
    fn from(p: Position) -> Direction {
        (p.x, p.y).into()
    }
}

#[derive(Clone, Copy)]
pub struct Glyph {
    pub id: char
}
impl Component for Glyph {
    type Storage = VecStorage<Self>;
}

#[derive(Clone, Copy)]
pub struct Lit;
impl Component for Lit {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Clone, Copy)]
pub struct Active;
impl Component for Active {
    type Storage = FlaggedStorage<Self>;
}

#[derive(Clone, Copy)]
pub struct Structure {
    pub light_required: i32,
    pub power_required: i32
}
impl Component for Structure {
    type Storage = FlaggedStorage<Self>;
}

#[derive(Clone, Copy)]
pub struct Glowsuit {
    pub power: i32
}
impl Component for Glowsuit {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Clone, Copy)]
pub struct LightSource {
    pub amount: i32,
    pub radius: i32
}
impl Component for LightSource {
    type Storage = VecStorage<Self>;
}