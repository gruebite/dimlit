#!/bin/sh -x

env GOOS=windows GOARCH=386 go build -v gitlab.com/gruebite/dimlit
zip dimlit-windows-i386-7drl.zip dimlit.exe README.txt
env GOOS=windows GOARCH=amd64 go build -v gitlab.com/gruebite/dimlit
zip dimlit-windows-x86_64-7drl.zip dimlit.exe README.txt

env GOOS=darwin GOARCH=386 go build -v gitlab.com/gruebite/dimlit
tar fcz dimlit-osx-i386-7drl.tar.gz dimlit README.txt
env GOOS=darwin GOARCH=amd64 go build -v gitlab.com/gruebite/dimlit
tar fcz dimlit-osx-x86_64-7drl-sound.tar.gz data/* dimlit README.txt

env GOOS=linux GOARCH=386 go build -v gitlab.com/gruebite/dimlit
tar fcz dimlit-linux-i386-7drl.tar.gz dimlit README.txt
env GOOS=linux GOARCH=amd64 go build -v gitlab.com/gruebite/dimlit
tar fcz dimlit-linux-x86_64-7drl.tar.gz dimlit README.txt

rm dimlit dimlit.exe

./butler push dimlit-windows-i386-7drl.zip gruebite/dimlit:windows-i386-7drl
./butler push dimlit-windows-x86_64-7drl.zip gruebite/dimlit:windows-x86_64-7drl
./butler push dimlit-osx-i386-7drl.tar.gz gruebite/dimlit:osx-i386-7drl
./butler push dimlit-osx-x86_64-7drl-sound.tar.gz gruebite/dimlit:osx-x86_64-7drl
./butler push dimlit-linux-i386-7drl.tar.gz gruebite/dimlit:linux-i386-7drl
./butler push dimlit-linux-x86_64-7drl.tar.gz gruebite/dimlit:linux-x86_64-7drl
