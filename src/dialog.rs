
use act::*;

pub struct DialogItem<T> {
    text: String,
    selectable: bool,
    returns: Option<T>,
    subdialog: Option<DialogFrame<T>>
}

impl<T> DialogItem<T> {
    pub fn text(&self) -> &str {
        &self.text
    }

    pub fn has_subdialog(&self) -> bool {
        self.subdialog.is_some()
    }
}

pub struct DialogFrame<T> {
    selected: Option<usize>,
    items: Vec<DialogItem<T>>
}

impl<T> DialogFrame<T> {
    pub fn new() -> DialogFrame<T> {
        DialogFrame {
            selected: None,
            items: vec![]
        }
    }

    pub fn add_item(&mut self, text: &str, selectable: bool, returns: Option<T>, subdialog: Option<DialogFrame<T>>) {
        self.items.push(DialogItem { text: text.to_owned(), selectable, returns, subdialog });
    }

    pub fn item(&self, idx: usize) -> (&DialogItem<T>, bool) {
        let selected = if let Some(i) = self.selected {
            i == idx
        } else {
            false
        };
        (&self.items[idx], selected)
    }

    fn item_mut(&mut self, idx: usize) -> (&mut DialogItem<T>, bool) {
        let selected = if let Some(i) = self.selected {
            i == idx
        } else {
            false
        };
        (&mut self.items[idx], selected)
    }

    pub fn item_count(&self) -> usize {
        self.items.len()
    }

    fn select_first_item(&mut self) {
        for (i, item) in self.items.iter().enumerate() {
            if item.selectable {
                self.selected = Some(i);
                break;
            }
        }
    }

    fn select_next_item(&mut self) {
        if let Some(idx) = self.selected {
            let mut i = (idx + 1) % self.item_count();
            while i != idx {
                if self.items[i].selectable {
                    self.selected = Some(i);
                    break;
                }
                i = (i + 1) % self.item_count();
            }
        }
    }

    fn select_prev_item(&mut self) {
        if let Some(idx) = self.selected {
            let mut i = (idx as isize) - 1;
            if i < 0 {
                i = (self.item_count() as isize) - 1;
            }
            while (i as usize) != idx {
                if self.items[i as usize].selectable {
                    self.selected = Some(i as usize);
                    break;
                }

                i -= 1;
                if i < 0 {
                    i = (self.item_count() as isize) - 1;
                }
            }
        }
    }
}

pub struct Dialog<T> {
    top: DialogFrame<T>,
    frames: Option<Vec<usize>>
}

impl<T: Copy> Dialog<T> {
    pub fn new(mut top: DialogFrame<T>) -> Dialog<T> {
        top.select_first_item();
        Dialog {
            top,
            frames: Some(vec![])
        }
    }

    pub fn activate(&mut self) {
        self.frames = Some(vec![]);
    }

    pub fn deactivate(&mut self) {
        self.frames = None;
    }

    pub fn is_active(&self) -> bool {
        self.frames.is_some()
    }

    pub fn current_frame(&self) -> Option<&DialogFrame<T>> {
        self.frames.as_ref().map(|frames| {
            let mut curr = &self.top;
            for &i in frames.iter() {
                curr = curr.item(i).0.subdialog.as_ref().unwrap();
            }
            curr
        })
    }

    fn current_frame_mut(&mut self) -> Option<&mut DialogFrame<T>> {
        let frames = self.frames.clone();
        frames.as_ref().map( move |frames| {
            let mut curr = &mut self.top;
            for &i in frames.iter() {
                curr = {curr}.item_mut(i).0.subdialog.as_mut().take().unwrap();
            }
            curr
        })
    }

    pub fn process(&mut self, act: Action) -> Option<T> {
        match act {
            Action::Right | Action::Enact => {
                let selected = self.current_frame_mut().unwrap().selected;
                let (idx, ret) = {
                    if let Some(idx) = selected {
                        let (item, _) = self.current_frame_mut().unwrap().item_mut(idx);
                        if let Some(ref mut subdialog) = item.subdialog {
                            subdialog.select_first_item();
                            (Some(idx), None)
                        } else {
                            (None, item.returns)
                        }
                    } else {
                        (None, None)
                    }
                };
                if let Some(idx) = idx {
                    self.frames.as_mut().unwrap().push(idx);
                } else {
                    self.deactivate();
                    return ret;
                }
            }
            Action::Left => {
                if self.frames.as_ref().unwrap().len() == 0 {
                    self.frames = None;
                } else {
                    self.frames.as_mut().unwrap().pop();
                }
            }
            Action::Up => {
                let cur = self.current_frame_mut().unwrap();
                cur.select_prev_item();
            }
            Action::Down => {
                let cur = self.current_frame_mut().unwrap();
                cur.select_next_item();
            }
            Action::Escape => {
                self.frames = None;
            }
            _ => {}
        }
        None
    }
}

macro_rules! frame {
    ( $(($text:expr, $sel:expr, $ret:expr, $sub:expr)),* ) => {{
        let mut df = DialogFrame::new();
        $(
            df.add_item($text, $sel, $ret, $sub);
        )*
        df
    }}
}
