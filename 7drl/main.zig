const c = @cImport({
    // See https://github.com/zig-lang/zig/issues/515
    @cDefine("_NO_CRT_STDIO_INLINE", "1");
    @cInclude("termbox.h");
});

pub fn main() void {
    const _ = c.tb_init();
    defer c.tb_shutdown();

    while (true) {
        c.tb_clear();
        const fg = c.TB_RED;
        const bg = c.TB_DEFAULT;
        c.tb_change_cell(8, 8, '@', fg, bg);
        c.tb_present();

        var ev: c.tb_event = undefined;
        if (c.tb_poll_event(&ev) >= 0) {
            switch (ev.ch) {
                'Q' => break,
                else => {}
            }
        }
    }
}
