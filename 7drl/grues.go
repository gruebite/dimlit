package main

import (
	"fmt"
	"math"
	"math/rand"

	"github.com/olebedev/emitter"
)

const grueSpawnRadius = 48
const grueSpawnTimer = 12

const grueEnragePercentageDenom = 5
const grueEnrageChance = 0.001
const grueEnrageChanceOffmap = 0.002
const grueNumberOfEnragedBeforeUrgrue = 30

type grueState int

const (
	grueNormal grueState = iota
	grueWarrior
	grueSiege
	grueLightSiege
	grueUr
	grueStateCount
)

type grueSubState int

const (
	grueWandering grueSubState = iota
	grueSeeking
	grueFleeing
)

func calcGrueMaxPopulation(turn int, diff difficultyLevel) int {
	return int(calcScale(turn, diff)) + 1
}

type grueWorldView interface {
	rng() *rand.Rand
	lightLevel(p point) int
	sageLoc() point
	bumpSage(damage, power int)
	killSage(message string)
}

type grueBehavior interface {
	update(w *world, gs *grues, g *grue)
}

type grueStateStats struct {
	enrageWeight int
	lightToKill  int
	minDamage    int
	maxDamage    int
	weights      dirWeightTable
	newBehavior  func() grueBehavior
}

var grueStates = [grueStateCount]grueStateStats{
	{0, 500, 5, 10, dirWeightTable{to: 6, diagonalTo: 5, neutral: 5, diagonalAway: 5, away: 5}, newNormalGrueBehavior},
	{10, 500, 5, 10, dirWeightTable{to: 10, diagonalTo: 5, neutral: 2, diagonalAway: 1, away: 0}, newWarriorGrueBehavior},
	{15, 500, 5, 25, dirWeightTable{to: 10, diagonalTo: 5, neutral: 2, diagonalAway: 2, away: 1}, newSiegeGrueBehavior},
	{1, 500, 5, 10, dirWeightTable{to: 5, diagonalTo: 5, neutral: 5, diagonalAway: 5, away: 5}, newAngryGrueBehavior},
	{0, 500, 5, 10, dirWeightTable{to: 10, diagonalTo: 5, neutral: 2, diagonalAway: 2, away: 5}, newUrGrueBehavior},
}

type grue struct {
	state    grueState
	substate grueSubState
	loc      point
	behavior grueBehavior
}

func newGrue(state grueState) *grue {
	return &grue{
		state,
		grueWandering,
		point{0, 0},
		grueStates[state].newBehavior(),
	}
}

func (g *grue) enrage(rng *rand.Rand) {
	weights := 0
	for _, gk := range grueStates {
		weights += gk.enrageWeight
	}
	r := rng.Intn(weights)
	for st, gk := range grueStates {
		if r < gk.enrageWeight {
			g.switchTo(grueState(st))
			return
		}
		r -= gk.enrageWeight
	}
}

func (g *grue) enraged() bool {
	return g.state != grueNormal
}

func (g *grue) switchTo(state grueState) {
	g.state = state
	g.behavior = grueStates[state].newBehavior()
	g.substate = grueWandering
}

func (g *grue) canSpawnOnLight(level int) bool {
	if level < grueStates[g.state].lightToKill {
		return true
	}
	return g.state != grueNormal
}

type grues struct {
	offMap     []*grue
	onMap      []*grue
	lookup     map[point][]*grue
	spawnTimer int
	rageCount  int
	urgrue     *grue
	e          *emitter.Emitter
}

func newGrues(e *emitter.Emitter) *grues {
	return &grues{
		make([]*grue, 0),
		make([]*grue, 0),
		make(map[point][]*grue),
		grueSpawnTimer,
		0,
		nil,
		e,
	}
}

func (gs *grues) placeGrue(g *grue) {
	gs.onMap = append(gs.onMap, g)
	_, exists := gs.lookup[g.loc]
	if !exists {
		gs.lookup[g.loc] = make([]*grue, 0)
	}
	gs.lookup[g.loc] = append(gs.lookup[g.loc], g)
}

func (gs *grues) spawnGrue(w *world, g *grue) {
	p := point{
		roll(w.rng, -grueSpawnRadius, grueSpawnRadius),
		roll(w.rng, -grueSpawnRadius, grueSpawnRadius),
	}
	l := w.sage.loc.add(p)
	if !g.canSpawnOnLight(w.lightLevel(l)) {
		gs.offMap = append(gs.offMap, g)
		return
	}
	g.loc = l
	gs.placeGrue(g)
}

func (gs *grues) gruePopulationOffMap() int {
	return len(gs.offMap)
}

func (gs *grues) gruePopulationOnMap() int {
	return len(gs.onMap)
}

func (gs *grues) gruePopulation() int {
	return len(gs.offMap) + len(gs.onMap)
}

func (gs *grues) countGruesAt(loc point) int {
	return len(gs.lookup[loc])
}

func (gs *grues) gruesAt(loc point) []*grue {
	return gs.lookup[loc]
}

func (gs *grues) gruesNearestTo(loc point) []*grue {
	if gs.gruePopulationOnMap() <= 0 {
		return nil
	}

	var closest []*grue
	var closestDist = math.MaxInt32
	for l, glist := range gs.lookup {
		if int(loc.distanceTo(l)) < closestDist {
			closest = glist
			closestDist = int(loc.distanceTo(l))
		}
	}
	return closest
}

func (gs *grues) _enrage(w *world) {
	// Enrage, moving to offmap should move enraged grues automatically
	pop := gs.gruePopulation()
	if pop == 0 {
		return
	}
	gs.e.Emit("message:You hear growls.", nil)

	rage := max(1, pop/grueEnragePercentageDenom)

	onRage := min(rage, gs.gruePopulationOnMap())
	rage -= onRage
	offRage := min(rage, gs.gruePopulationOffMap())

	for _, g := range gs.onMap {
		if onRage <= 0 {
			break
		}
		if !g.enraged() {
			onRage--
			g.enrage(w.rng)
		}
	}

	for _, g := range gs.offMap {
		if offRage <= 0 {
			break
		}
		if !g.enraged() {
			offRage--
			g.enrage(w.rng)
			gs.spawnGrue(w, g)
		}
	}
}

func (gs *grues) moveGrue(w *world, delta point) {

}

func (gs *grues) update(w *world) {
	if gs.gruePopulation() < calcGrueMaxPopulation(w.turnCount, w.diff) {
		gs.spawnTimer--
		if gs.spawnTimer <= 0 {
			gs.spawnGrue(w, newGrue(grueNormal))
			gs.spawnTimer = grueSpawnTimer
		}
	}

	enrageChance := grueEnrageChance
	if len(gs.onMap) == 0 && len(gs.offMap) > 0 {
		enrageChance = grueEnrageChanceOffmap
	}

	if w.turnCount >= gracePeriod && w.rng.Float64() < enrageChance {
		gs._enrage(w)
		gs.rageCount++
		if gs.rageCount%grueNumberOfEnragedBeforeUrgrue == 0 {
			// Skip every other 30
			if gs.urgrue != nil {
				gs.urgrue = newGrue(grueUr)
			} else {
				gs.urgrue = nil
			}
		}
	}

	if gs.urgrue != nil {
		gs.urgrue.behavior.update(w, gs, gs.urgrue)
		gs.urgrue.behavior.update(w, gs, gs.urgrue)
		gs.urgrue.behavior.update(w, gs, gs.urgrue)
	}

	oldOffMap := gs.offMap
	gs.offMap = make([]*grue, 0)
	for _, g := range oldOffMap {
		gs.spawnGrue(w, g)
	}

	oldMap := gs.onMap
	gs.onMap = make([]*grue, 0)
	gs.lookup = make(map[point][]*grue)
	for _, g := range oldMap {
		if g.loc.distanceTo(w.sage.loc) >= grueSpawnRadius {
			gs.spawnGrue(w, g)
			continue
		}
		g.behavior.update(w, gs, g)
	}
}

type normalGrueBehavior struct {
}

func newNormalGrueBehavior() grueBehavior {
	return &normalGrueBehavior{}
}

func (b *normalGrueBehavior) update(w *world, gs *grues, g *grue) {
	if w.lightLevel(g.loc) >= grueStates[g.state].lightToKill {
		w.e.Emit("message:A grue was killed.", nil)
		w.e.Emit("event:grueDeath", g.loc)
		// Dead
		return
	}

	// Running from light is highest priorty.
	if w.isLit(g.loc) {
		dark := w.findNearestDark(g.loc)
		heading := dark.add(g.loc.neg()).dir8()
		g.loc = g.loc.add(heading)
		gs.placeGrue(g)
		return
	}

	// When the sage walks on or is bumped onto us. Just move randomly.
	if w.sage.loc == g.loc {
		dir := randomDirection8(w.rng)
		new := g.loc.add(dir)
		g.loc = new
		gs.placeGrue(g)
		return
	}

	dir := w.sage.loc.add(g.loc.neg())
	new := g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	if w.sage.loc == new {
		dmg := roll(w.rng, grueStates[g.state].minDamage, grueStates[g.state].maxDamage)
		w.bumpSageInDir(dmg, dir)
		gs.placeGrue(g)
		return
	}
	if w.structureAt(new) != nil {
		s := w.structureAt(new)
		if !s.broken {
			s.damage(w, true)
			w.e.Emit(fmt.Sprintf("message:A grue has damaged %v.", s), nil)
		}
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	g.loc = new
	gs.placeGrue(g)
}

type warriorGrueBehavior struct {
	attackCount int
}

func newWarriorGrueBehavior() grueBehavior {
	return &warriorGrueBehavior{}
}

func (b *warriorGrueBehavior) update(w *world, gs *grues, g *grue) {
	if w.lightLevel(g.loc) >= grueStates[g.state].lightToKill {
		w.e.Emit("message:A grue was killed.", nil)
		w.e.Emit("event:grueDeath", g.loc)
		// Dead
		return
	}

	if g.substate == grueWandering {
		g.substate = grueSeeking
	}

	if b.attackCount >= 2 {
		g.switchTo(grueNormal)
		return
	}

	// Running from light is highest priorty only when fleeing.
	if g.substate == grueFleeing && w.isLit(g.loc) {
		dark := w.findNearestDark(g.loc)
		heading := dark.add(g.loc.neg()).dir8()
		g.loc = g.loc.add(heading)
		gs.placeGrue(g)
		return
	} else if g.substate == grueFleeing {
		// No longer in danger. Become nice.
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	dir := w.sage.loc.add(g.loc.neg())
	new := g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	if w.sage.loc == new {
		dmg := roll(w.rng, grueStates[g.state].minDamage, grueStates[g.state].maxDamage)
		w.bumpSageInDir(dmg, dir)
		gs.placeGrue(g)
		b.attackCount++
		return
	}
	if w.structureAt(new) != nil {
		s := w.structureAt(new)
		if s.broken {
			w.destroyStructureAt(new)
			w.e.Emit(fmt.Sprintf("message:A grue has destroyed %v.", s), nil)
		} else {
			s.damage(w, true)
			w.e.Emit(fmt.Sprintf("message:A grue has damaged %v.", s), nil)
		}
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	g.loc = new
	gs.placeGrue(g)
}

type siegeGrueBehavior struct {
	targetStructure int
}

func newSiegeGrueBehavior() grueBehavior {
	return &siegeGrueBehavior{-1}
}

func (b *siegeGrueBehavior) update(w *world, gs *grues, g *grue) {
	if w.lightLevel(g.loc) >= grueStates[g.state].lightToKill {
		w.e.Emit("message:A grue was killed.", nil)
		w.e.Emit("event:grueDeath", g.loc)
		// Dead
		return
	}

	if g.substate == grueWandering {
		g.substate = grueSeeking
		b.targetStructure = w.findNearestStructure(w.sage.loc).id
	}

	// Running from light is highest priorty only when fleeing.
	if g.substate == grueFleeing && w.isLit(g.loc) {
		dark := w.findNearestDark(g.loc)
		heading := dark.add(g.loc.neg()).dir8()
		g.loc = g.loc.add(heading)
		gs.placeGrue(g)
		return
	} else if g.substate == grueFleeing {
		// No longer in danger. Become nice.
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	s := w.findStructureByID(b.targetStructure)
	if s == nil {
		g.substate = grueWandering
		gs.placeGrue(g)
		return
	}

	dir := s.loc.add(g.loc.neg())
	new := g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	if w.sage.loc == new {
		dmg := roll(w.rng, grueStates[g.state].minDamage, grueStates[g.state].maxDamage)
		w.bumpSageInDir(dmg, dir)
		gs.placeGrue(g)
		return
	}
	if w.structureAt(new) != nil {
		s := w.structureAt(new)
		if s.broken {
			w.destroyStructureAt(new)
			w.e.Emit(fmt.Sprintf("message:A grue has destroyed %v.", s), nil)
		} else {
			s.damage(w, true)
			w.e.Emit(fmt.Sprintf("message:A grue has damaged %v.", s), nil)
		}
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	g.loc = new
	gs.placeGrue(g)
}

type angryGrueBehavior struct {
}

func newAngryGrueBehavior() grueBehavior {
	return &angryGrueBehavior{}
}

func (b *angryGrueBehavior) update(w *world, gs *grues, g *grue) {
	if w.lightLevel(g.loc) >= grueStates[g.state].lightToKill {
		w.e.Emit("message:A grue was killed.", nil)
		w.e.Emit("event:grueDeath", g.loc)
		// Dead
		return
	}

	s := w.findNearestStructure(g.loc)
	var dir point
	var new point
	if w.sage.loc.distanceTo(g.loc) > s.loc.distanceTo(g.loc) {
		dir = w.sage.loc.add(g.loc.neg())
		new = g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	} else {
		dir = s.loc.add(g.loc.neg())
		new = g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	}
	if w.sage.loc == new {
		dmg := roll(w.rng, grueStates[g.state].minDamage, grueStates[g.state].maxDamage)
		w.bumpSageInDir(dmg, dir)
		gs.placeGrue(g)
		return
	}
	if w.structureAt(new) != nil {
		s := w.structureAt(new)
		if s.broken {
			w.destroyStructureAt(new)
			w.e.Emit(fmt.Sprintf("message:A grue has destroyed %v.", s), nil)
		} else {
			s.damage(w, true)
			w.e.Emit(fmt.Sprintf("message:A grue has damaged %v.", s), nil)
		}
		g.switchTo(grueNormal)
		gs.placeGrue(g)
		return
	}

	g.loc = new
	gs.placeGrue(g)
}

type urGrueBehavior struct {
	initialized bool
	life        int
}

func newUrGrueBehavior() grueBehavior {
	return &urGrueBehavior{false, 60}
}

func (b *urGrueBehavior) update(w *world, gs *grues, g *grue) {
	if !b.initialized {
		b.initialized = true
		w.e.Emit("message:The Ur-grue has spawned.", nil)
		w.e.Emit("event:urgrueSpawn", g.loc)
	}

	b.life--
	w.e.Emit(fmt.Sprintf("message:%d", b.life), nil)
	if b.life <= 0 {
		if b.life == 0 {
			w.e.Emit("message:The Ur-grue has has disappeared.", nil)
			w.e.Emit("event:urgrueDespawn", g.loc)
		}
		gs.urgrue = nil
		return
	}

	dir := w.sage.loc.add(g.loc.neg())
	new := g.loc.add(dir.randomWeighted(w.rng, grueStates[g.state].weights))
	if w.sage.loc == new {
		dmg := roll(w.rng, grueStates[g.state].minDamage, grueStates[g.state].maxDamage)
		w.bumpSageInDir(dmg, dir)
		return
	}
	if w.structureAt(new) != nil {
		s := w.structureAt(new)
		w.destroyStructureAt(new)
		w.e.Emit(fmt.Sprintf("message:The ur-grue has destroyed %v.", s), nil)
		return
	}

	g.loc = new
}
