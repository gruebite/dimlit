
use bear_lib_terminal::terminal::{Event, KeyCode};

#[derive(PartialEq, Eq)]
pub enum Action {
    None, Up, Down, Left, Right, Enact, Escape
}

impl From<Event> for Action {
    fn from(ev: Event) -> Self {
        match ev {
            Event::KeyPressed{key: KeyCode::Q, ctrl: false, shift: false} => Action::Escape,
            Event::KeyPressed{key: KeyCode::H, ctrl: false, shift: false} => Action::Left,
            Event::KeyPressed{key: KeyCode::J, ctrl: false, shift: false} => Action::Down,
            Event::KeyPressed{key: KeyCode::K, ctrl: false, shift: false} => Action::Up,
            Event::KeyPressed{key: KeyCode::L, ctrl: false, shift: false} => Action::Right,
            Event::KeyPressed{key: KeyCode::Space, ctrl: false, shift: false} => Action::Enact,
            _ => Action::None
        }
    }
}