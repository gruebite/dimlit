
#include <string.h>

#include "tagset.h"

static int count_bits(uint32_t i)
{
     i = i - ((i >> 1) & 0x55555555);
     i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
     return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

int ts_count_tags(struct tagset *ts)
{
        int count = 0;
        for (int i = 0; i < TS_BLOCK_COUNT; ++i) {
                count += count_bits(ts->tags[i]);
        }
        return count;
}

bool ts_has_tag(struct tagset *ts, int t)
{
        int idx = t / 32;
        int shift = t % 32;
        return ts->tags[idx] & (1 << shift);
}

void ts_set_tag(struct tagset *ts, int t)
{
        int idx = t / 32;
        int shift = t % 32;
        ts->tags[idx] |= (1 << shift);
}

void ts_union(struct tagset *dst, struct tagset *ts0, struct tagset *ts1)
{
        memset(dst, 0, sizeof(*dst));
        for (int i = 0; i < TS_BLOCK_COUNT; ++i) {
                dst->tags[i] = ts0->tags[i] | ts1->tags[i];
        }
}

void ts_intersect(struct tagset *dst, struct tagset *ts0, struct tagset *ts1)
{
        memset(dst, 0, sizeof(*dst));
        for (int i = 0; i < TS_BLOCK_COUNT; ++i) {
                dst->tags[i] = ts0->tags[i] & ts1->tags[i];
        }
}

void ts_subtract(struct tagset *dst, struct tagset *ts0, struct tagset *ts1)
{
        memset(dst, 0, sizeof(*dst));
        for (int i = 0; i < TS_BLOCK_COUNT; ++i) {
                dst->tags[i] = ts0->tags[i] & (~ts1->tags[i]);
        }
}

#ifdef UNITTEST
#include <stdio.h>

int main(void)
{
        struct tagset ts0 = {0};
        struct tagset ts1 = {0};
        struct tagset dst = {0};
        ts_set_tag(&ts0, 36);
        if (!ts_has_tag(&ts0, 36)) {
                return 1;
        }
        ts_set_tag(&ts1, 36);
        ts_subtract(&dst, &ts0, &ts1);
        if (ts_count_tags(&dst) > 0) {
                return 1;
        }
        return 0;
}

#endif
