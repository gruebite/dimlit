
use std;

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum Direction {
    None,
    North,
    Northwest,
    West,
    Southwest,
    South,
    Southeast,
    East,
    Northeast
}

impl Direction {
    fn cardinal() -> [Direction; 4] {
        use self::Direction::*;
        [
            North,
            West,
            South,
            East
        ]
    }

    fn intercardinal() -> [Direction; 8] {
        use self::Direction::*;
        [
            South,
            Southeast,
            East,
            Northeast,
            North,
            Northwest,
            West,
            Southwest,
        ]
    }

    fn dx(self) -> i32 {
        use self::Direction::*;
        match self {
            East | Northeast | Southeast => 1,
            West | Northwest | Southwest => -1,
            _ => 0,
        }
    }

    fn dy(self) -> i32 {
        use self::Direction::*;
        match self {
            North | Northeast | Northwest => -1,
            South | Southeast | Southwest => 1,
            _ => 0,
        }
    }

    fn opposite(self) -> Direction {
        use self::Direction::*;
        match self {
            North => South,
            South => North,
            East => West,
            West => East,
            Northeast => Southwest,
            Southwest => Northeast,
            Northwest => Southeast,
            Southeast => Northwest,
            None => None,
        }
    }
}

impl From<(i32, i32)> for Direction {
    fn from((dx, dy): (i32, i32)) -> Direction {
        let angle = (dx as f64).atan2(dy as f64);
        let octant = (8.0 * angle / (2.0 * std::f64::consts::PI)).round() + 8.0;
        let octant = (octant as usize) % 8;
        Direction::intercardinal()[octant]
    }
}

#[cfg(test)]
mod test {
    use super::{*, Direction::*};

    #[test]
    fn test_from() {
        assert_eq!(East, (1, 0).into());
        assert_eq!(North, (0, -1).into());
        assert_eq!(Southwest, (-1, 1).into());
        assert_eq!(Southwest, (-10, 10).into());
        assert_eq!(Southwest, (-10, 9).into());
        assert_eq!(Southwest, (-10, 8).into());
        assert_eq!(Southwest, (-10, 7).into());
        assert_eq!(Southwest, (-10, 6).into());
        assert_eq!(Southwest, (-10, 5).into());
        assert_eq!(West, (-10, 4).into());
    }

    #[test]
    fn test_opposite() {
        for &dir in Direction::cardinal().iter() {
            assert_eq!(dir, dir.opposite().opposite());
        }
        for &dir in Direction::intercardinal().iter() {
            assert_eq!(dir, dir.opposite().opposite());
        }
    }
}
