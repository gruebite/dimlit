
#ifndef MESSAGING_H
#define MESSAGING_H

void messaging_init_or_die(int max_length);
void messaging_start_turn(void);
void messaging_push(const char *fmt, ...);

#endif
