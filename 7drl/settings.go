package main

import "math"

type difficultyLevel int

const (
	quasarDifficulty difficultyLevel = iota
	sunDifficulty
	blackHoleDifficulty
	difficultyCount
)

var difficultySettings = []int{
	0,
	2000,
	5000,
}

type gameMode int

const (
	survivalGameMode gameMode = iota
	sandboxGameMode
	journeyGameMode
	gameModeCount
)

const gracePeriod = 200

const sageGlowsuitMaxPower = 100
const sageGlowsuitCooldown = 120
const sageGlowsuitWarn = 25
const sageMaxCrystals = 100
const sageMaxRunSpeed = 5
const sageSightRange = 7
const sageRunningCostPerSpeed = 1
const sageHearingRange = 25

const crystalLifetime = 2000
const crystalSpawnChance = 0.03
const crystalMin = 1
const crystalMax = 4
const grueWarnsAfter = 1
const grueKillsAfter = 21

const scaleMultiplier = 20
const scaleBase = math.E

func calcScale(turn int, diff difficultyLevel) float64 {
	fturn := float64(max(turn, 2) + difficultySettings[diff])
	return fturn / ((math.Log(fturn) / math.Log(scaleBase)) * scaleMultiplier)
}
