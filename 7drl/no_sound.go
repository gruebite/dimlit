// +build linux darwin,386 windows,386

package main

import (
	"math/rand"
	"time"

	"github.com/olebedev/emitter"
)

type world struct {
	rng          *rand.Rand
	diff         difficultyLevel
	sage         sage
	nextID       int
	structures   map[point]*structure
	lit          map[point]int
	protected    map[point]int
	crystalSpots map[point]*crystalSpot
	darkness     *darkness
	messages     []string
	messageAdded bool
	turnCount    int
	e            *emitter.Emitter
	messaging    *messagingSystem
	grues        *grues
}

func newWorld() *world {
	w := new(world)
	w.rng = rand.New(rand.NewSource(time.Now().UnixNano()))
	w.sage.sight = sageSightRange
	w.sage.glowsuit = sageGlowsuitMaxPower
	w.sage.speed = 1
	w.sage.unlocks = make(map[structureCategoryKind]int)
	w.sage.unlocks[structureTier0] = 1
	w.structures = make(map[point]*structure)
	w.lit = make(map[point]int)
	w.protected = make(map[point]int)
	w.crystalSpots = make(map[point]*crystalSpot)
	w.crystalSpots[point{0, -6}] = &crystalSpot{16, 0, crystalLifetime}
	w.darkness = newDarkness()
	w.messages = make([]string, 0)
	w.addStructure(structureLife, point{0, 1})
	w.turnCount = 0
	w.e = &emitter.Emitter{}
	w.e.Use("*", emitter.Void)
	w.messaging = newMessaging(w.e, &w.sage)
	w.grues = newGrues(w.e)
	w.update()
	return w
}
