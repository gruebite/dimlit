#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "vector.h"

#define GROWTH_RATE 1.6

static void *aligned_malloc(size_t align, size_t size)
{
	void *mem = malloc(size + (align - 1) + sizeof(void *));
	if (!mem) {
		return NULL;
	}
	char *amem = ((char *)mem) + sizeof(void *);
	amem += (align - ((uintptr_t)amem & (align - 1)) & (align - 1));
	((void **)amem)[-1] = mem;
	return amem;
}

static void aligned_free(void *mem)
{
	if (!mem) {
		return;
	}
	free(((void **)mem)[-1]);
}

static int alloc_to_size(struct vector *v, size_t size)
{
	assert(v->length * v->elem_size <= size);
	void *new_data = /*aligned_*/malloc(/*v->elem_size, */size);
	if (!new_data) {
		return VECTOR_ERROR_NOMEM;
	}
	v->allocd = size;
	if (v->data) {
		memcpy(new_data, v->data, v->length * v->elem_size);
	}
	/*aligned_*/free(v->data);
	v->data = new_data;
	return 0;
}

static int grow(struct vector *v)
{
	size_t max_elems = v->allocd / v->elem_size;
	size_t new_max = max_elems * GROWTH_RATE;
	if (new_max <= max_elems) {
		++new_max;
	}
	return alloc_to_size(v, new_max * v->elem_size);
}

int vector_init(struct vector *v, size_t initial, size_t elem_size)
{
	v->allocd = 0;
	v->length = 0;
	v->elem_size = elem_size;
	if (initial > 0) {
		v->data = /*aligned_*/malloc(/*elem_size, */initial * elem_size);
		if (!v->data) {
			return VECTOR_ERROR_NOMEM;
		}
	} else {
		v->data = NULL;
	}
	v->allocd = initial * elem_size;
	v->_buffer = malloc(elem_size);
	if (!v->_buffer) {
		/*aligned_*/free(v->data);
		return VECTOR_ERROR_NOMEM;
	}
	return 0;
}

int vector_shrink(struct vector *v)
{
	return alloc_to_size(v, v->length * v->elem_size);
}

int vector_reserve(struct vector *v, size_t size)
{
	v->length = v->length < size ? size : v->length;
	return alloc_to_size(v, size * v->elem_size);
}

int vector_copy(struct vector *v, struct vector *out)
{
	if (vector_init(out, v->allocd / v->elem_size, v->elem_size) == -1) {
		return -1;
	}
	memcpy(out->data, v->data, v->allocd);
	out->length = v->length;
	return 0;
}

void vector_uninit(struct vector *v)
{
	if (!v) {
		return;
	}
	if (v->data) {
		/*aligned_*/free(v->data);
	}
	if (v->_buffer) {
		free(v->_buffer);
	}
	memset(v, 0, sizeof(*v));
}

int vector_push_back(struct vector *v, void *data)
{
	if ((v->length + 1) * v->elem_size >= v->allocd) {
		int rv = grow(v);
		if (rv != 0) {
			return rv;
		}
	}
	char *array = v->data;
	memcpy(&array[v->length * v->elem_size], data, v->elem_size);
	++v->length;
	return 0;
}

int vector_push_front(struct vector *v, void *data)
{
	if ((v->length + 1) * v->elem_size >= v->allocd) {
		int rv = grow(v);
		if (rv != 0) {
			return rv;
		}
	}
	char *array = v->data;
	memmove(&array[v->elem_size], array, v->length * v->elem_size);
	memcpy(&array[0], data, v->elem_size);
	++v->length;
	return 0;
}

void vector_pop_back(struct vector *v, void *out)
{
	if (out) {
		char *array = v->data;
		memcpy(out, &array[(v->length - 1) * v->elem_size], v->elem_size);
	}
	--v->length;
}

void vector_pop_front(struct vector *v, void *out)
{
	char *array = v->data;
	memmove(array, &array[v->elem_size], (v->length - 1) * v->elem_size);
	if (out) {
		memcpy(out, &array[0], v->elem_size);
	}
	--v->length;
}

int vector_ordered_insert(struct vector *v, void *data, int (*cmp)(const void *, const void *))
{
	int rv = vector_push_back(v, data);
	if (rv != 0) {
		return rv;
	}

	char *array = v->data;
	for (int curr = v->length - 1; curr >= 1; --curr) {
		int prev = curr - 1;
		void *curr_elem = &array[curr * v->elem_size];
		void *prev_elem = &array[prev * v->elem_size];
		if (cmp(curr_elem, prev_elem) >= 0) {
			/* Found element we're higher than. Stop. */
			break;
		}
		memcpy(v->_buffer, prev_elem, v->elem_size);
		memcpy(prev_elem, curr_elem, v->elem_size);
		memcpy(curr_elem, v->_buffer, v->elem_size);
	}
	return 0;
}

void vector_sort(struct vector *v, int (*cmp)(const void *, const void *))
{
	qsort(v, v->length, v->elem_size, cmp);
}

int vector_bsearch(struct vector *v, void *data, int (*cmp)(const void *, const void *), void *out)
{
	char *array = v->data;
	int i = 0;
	int j = v->length - 1;
	while (i <= j) {
		int k = (i + j) / 2;
		int c = cmp(&array[k * v->elem_size], data);
		if (c == 0) {
			if (out) {
				vector_get(v, k, out);
			}
			return k;
		}
		else if (c < 0) {
			i = k + 1;
		}
		else {
			j = k - 1;
		}
	}
	return -1;
}

void vector_get(struct vector *v, size_t idx, void *out)
{
	char *array = v->data;
	memcpy(out, &array[idx * v->elem_size], v->elem_size);
}

void vector_ref(struct vector *v, size_t idx, void **ref)
{
	char *array = v->data;
	*ref = &array[idx * v->elem_size];
}

void vector_set(struct vector *v, size_t idx, void *data)
{
	char *array = v->data;
	memcpy(&array[idx * v->elem_size], data, v->elem_size);
}

void vector_del(struct vector *v, size_t idx, void *out)
{
	char *array = v->data;
	if (out) {
		memcpy(out, &array[idx * v->elem_size], v->elem_size);
	}
	size_t leftover = (v->length - idx - 1) * v->elem_size;
	memmove(&array[idx * v->elem_size], &array[(idx + 1) * v->elem_size], leftover);
	--v->length;
}

#ifdef UNITTEST
#include <stdio.h>

static int cmp_int(const void *a, const void *b)
{
	return *(int *)a - *(int *)b;
}

int main(void)
{
	struct vector v;
	if (vector_init(&v, 0, sizeof(int)) == -1) {
		fprintf(stderr, "error: out of memory\n");
		return -1;
	}

	for (int i = 0; i < 10; ++i) {
		vector_push_back(&v, &i);
	}
	int in = 42;
	vector_set(&v, 5, &in);

	int out;
	vector_get(&v, 6, &out);
	printf("out=%d\n", out);
	vector_get(&v, 5, &out);
	printf("out=%d\n", out);
	int *outp;
	vector_ref(&v, 6, (void *)&outp);
	if (*outp != 6) {
		return 1;
	}

	vector_del(&v, 5, NULL);
	vector_pop_front(&v, NULL);
	vector_push_front(&v, &in);
	vector_shrink(&v);
	for (int i = 0; i < v.length; ++i) {
		vector_get(&v, i, &out);
		printf("[%d]=%d\n", i, out);
	}

	printf("v=%zu,%zu,%zu\n", v.allocd, v.length, v.elem_size);

	v.length = 0;
	
	int r;
	for (int i = 0; i < 10; ++i) {
		r = rand() % 100;
		printf("inserted %d\n", r);
		vector_ordered_insert(&v, &r, cmp_int);
	}

	if (vector_bsearch(&v, &r, cmp_int, NULL) < 0) {
		return -1;
	}

	struct vector copy;
	vector_copy(&v, &copy);
	if (vector_bsearch(&copy, &r, cmp_int, NULL) < 0) {
		return -1;
	}

	int first;
	vector_get(&v, 0, &first);
	for (int i = 1; i < v.length; i++) {
		int test;
		vector_get(&v, i, &test);
		if (first >= test) {
			printf("!!! %d >= %d\n", first, test);
			return 1;
		}
		printf("%d < %d\n", first, test);
		first = test;
	}

        vector_uninit(&v);
        
        return 0;
}
#endif