#ifndef TAGSET_H
#define TAGSET_H

#include <stdbool.h>
#include <stdint.h>

#define TS_BLOCK_COUNT 4

#define TS_MAX_TAGS (32 * TS_BLOCK_COUNT)

struct tagset {
        uint32_t tags[TS_BLOCK_COUNT];
};

#define TS_STATIC()

int ts_count_tags(struct tagset *ts);
bool ts_has_tag(struct tagset *ts, int t);
void ts_set_tag(struct tagset *ts, int t);
void ts_union(struct tagset *dst, struct tagset *ts0, struct tagset *ts1);
void ts_intersect(struct tagset *dst, struct tagset *ts0, struct tagset *ts1);
void ts_subtract(struct tagset *dst, struct tagset *ts0, struct tagset *ts1);

#endif
