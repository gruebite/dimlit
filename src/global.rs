

pub const WIDTH: i32 = 80;
pub const HEIGHT: i32 = 25;
pub const FIELD_WIDTH: i32 = WIDTH / 2;
pub const FIELD_HEIGHT: i32 = HEIGHT - 5;