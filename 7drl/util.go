package main

import (
	"fmt"
	"math"
	"math/rand"
	"strings"

	"github.com/nsf/termbox-go"
)

func wordWrap(text string, lineWidth int) string {
	words := strings.Fields(strings.TrimSpace(text))
	if len(words) == 0 {
		return text
	}
	wrapped := words[0]
	spaceLeft := lineWidth - len(wrapped)
	for _, word := range words[1:] {
		if len(word)+1 > spaceLeft {
			wrapped += "\n" + word
			spaceLeft = lineWidth - len(word)
		} else {
			wrapped += " " + word
			spaceLeft -= 1 + len(word)
		}
	}

	return wrapped
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

func roll(rng *rand.Rand, mn, mx int) int {
	return rng.Intn(mx-mn) + mn
}

func squish(s string, width int) string {
	if len(s) >= width {
		return s[:width]
	}
	return s[:]
}

type point struct {
	x, y int
}

var directions8 = [8]point{
	{0, 1},   // 0
	{1, 1},   // 1
	{1, 0},   // 2
	{1, -1},  // 3
	{0, -1},  // 4
	{-1, -1}, // 5
	{-1, 0},  // 6
	{-1, 1},  // 7
}

var directionsNames8 = [8]string{
	"south",
	"southeast",
	"east",
	"northeast",
	"north",
	"northwest",
	"west",
	"southwest",
}

func (p point) times(m int) point {
	return point{p.x * m, p.y * m}
}

func (p point) lineTo(other point) []point {
	ps := make([]point, 0)
	x0 := p.x
	y0 := p.y
	x1 := other.x
	y1 := other.y

	dx := x1 - x0
	if dx < 0 {
		dx = -dx
	}
	dy := y1 - y0
	if dy < 0 {
		dy = -dy
	}
	var sx, sy int
	if x0 < x1 {
		sx = 1
	} else {
		sx = -1
	}
	if y0 < y1 {
		sy = 1
	} else {
		sy = -1
	}
	err := dx - dy

	for {
		ps = append(ps, point{x0, y0})
		if x0 == x1 && y0 == y1 {
			break
		}
		e2 := 2 * err
		if e2 > -dy {
			err -= dy
			x0 += sx
		}
		if e2 < dx {
			err += dx
			y0 += sy
		}
	}

	return ps
}

func lerp(a, b, f float64) float64 {
	return a + f*(b-a)
}

func randomDirection8(rng *rand.Rand) point {
	return directions8[rng.Intn(len(directions8))]
}

type dirWeightTable struct {
	to           int
	diagonalTo   int
	neutral      int
	diagonalAway int
	away         int
}

func (dir point) randomWeighted(rng *rand.Rand, wt dirWeightTable) point {
	oct := dir.octant()

	chances := [8]int{}

	chances[oct] = wt.to
	chances[(oct+4)%8] = wt.away

	chances[(oct+1)%8] = wt.diagonalTo
	chances[(oct+5)%8] = wt.diagonalAway

	chances[(oct+7)%8] = wt.diagonalTo
	chances[(oct+11)%8] = wt.diagonalAway

	chances[(oct+2)%8] = wt.neutral
	chances[(oct+6)%8] = wt.neutral

	r := rng.Intn(wt.to + wt.away + wt.diagonalTo*2 + wt.diagonalAway*2 + wt.neutral)
	for i, ch := range chances {
		if r < ch {
			return directions8[i]
		}
		r -= ch
	}
	panic("bug")
}

func (p point) add(o point) point {
	return point{p.x + o.x, p.y + o.y}
}
func (p point) mul(m int) point {
	return point{p.x * m, p.y * m}
}

func (p point) mag2() int {
	return p.x*p.x + p.y*p.y
}

func (p point) neg() point {
	return point{-p.x, -p.y}
}

func (p point) distanceTo(o point) float64 {
	return math.Sqrt(float64(p.add(o.neg()).mag2()))
}

func (p point) within(topleft, botright point) bool {
	return p.x >= topleft.x && p.x <= botright.x && p.y >= topleft.y && p.y <= botright.y
}

func (p point) octant() int {
	angle := math.Atan2(float64(p.x), float64(p.y))
	octant := int(math.Round(8*angle/(2*math.Pi)+8)) % 8

	return octant
}

func (p point) dir8() point {
	return directions8[p.octant()]
}

func (p point) cardinal() string {
	return directionsNames8[p.octant()]
}

func (p point) String() string {
	return fmt.Sprintf("%d, %d", p.x, p.y)
}

func spiral(n int) (int, int) {
	k := math.Ceil((math.Sqrt(float64(n)) - 1) / 2)
	t := 2*k + 1
	m := math.Pow(t, 2)
	t = t - 1
	if float64(n) >= m-t {
		return int(k - (m - float64(n))), int(-k)
	}
	m = m - t
	if float64(n) >= m-t {
		return int(-k), int(-k + (m - float64(n)))
	}
	m = m - t
	if float64(n) >= m-t {
		return int(-k + (m - float64(n))), int(k)
	}
	return int(k), int(k - (m - float64(n) - t))
}

func tbPrint(x, y int, fg, bg termbox.Attribute, msg string) {
	for _, c := range msg {
		termbox.SetCell(x, y, c, fg, bg)
		x++
	}
}

func tbPrintf(x, y int, fg, bg termbox.Attribute, format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	tbPrint(x, y, fg, bg, s)
}

func dimPrint(x, y int, msg string) {
	fg := termbox.ColorDefault
	bg := termbox.ColorDefault
	for _, c := range msg {
		if c == '[' || c == ']' {
			fg ^= termbox.AttrReverse
			continue
		}
		termbox.SetCell(x, y, c, fg, bg)
		x++
	}
}

func dimPrintf(x, y int, format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	dimPrint(x, y, s)
}

func dimPrintLen(msg string) int {
	l := 0
	for _, c := range msg {
		if c != '[' && c != ']' {
			l++
		}
	}
	return l
}

func dimPrintfLen(format string, args ...interface{}) int {
	s := fmt.Sprintf(format, args...)
	return dimPrintLen(s)
}

func drawReverseLine(a, b, topleft, botright point) {
	line := a.lineTo(b)
	cells := termbox.CellBuffer()
	width, _ := termbox.Size()
	for _, p := range line {
		x := p.x
		y := p.y
		if x < topleft.x || x >= botright.x || y < topleft.y || y >= botright.y {
			continue
		}
		cells[y*width+x].Fg ^= termbox.AttrReverse
	}
}
