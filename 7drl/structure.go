package main

import "fmt"

const maxLinks = 5

type structureBehavior interface {
	link(w *world, s *structure, id int)
	unlink(w *world, s *structure, slot int)
	activate(w *world, s *structure)
	deactivate(w *world, s *structure)
	update(w *world, s *structure)
	actionList(s *structure) []string
	doAction(w *world, s *structure, i int)
	infoList(w *world, s *structure) []string
}

type noLinkBehavior struct{}

func (noLinkBehavior) link(w *world, s *structure, id int) {}

type noUnlinkBehavior struct{}

func (noUnlinkBehavior) unlink(w *world, s *structure, slot int) {}

type noActivateBehavior struct{}

func (noActivateBehavior) activate(w *world, s *structure) {}

type noDeactivateBehavior struct{}

func (noDeactivateBehavior) deactivate(w *world, s *structure) {}

type noUpdateBehavior struct{}

func (noUpdateBehavior) update(w *world, s *structure) {}

type noActionListBehavior struct{}

func (noActionListBehavior) actionList(s *structure) []string { return nil }

type noDoActionBehavior struct{}

func (noDoActionBehavior) doAction(w *world, s *structure, i int) {}

type noInfoListBehavior struct{}

func (noInfoListBehavior) infoList(w *world, s *structure) []string { return nil }

var structureInfo = []string{
	"A powerful efficient structure providing light and info.",
	"Powers other structures by linking to them.",
	"Three times more powerful than Power Structure.",
	"Provides light in a radius.",
	"Provides light in a north direction.",
	"Provides light in an east direction.",
	"Provides light in a south direction.",
	"Provides light in a west direction.",
	"Great for distracting grues.",
	"Passively charges your glowsuit when in radius.",
	"Crystals materialize more frequently within radius.",
	"A pure structure that stores crystals. Deconstruct to get them back.",
	"Automatically repairs broken structures that are linked.",
	"Provides a small light and cheap light.",
	"Fires a beam of intense light at nearby grues. Has a long cooldown.",
	"Tells time and outputs a general scale of how intense the darkness is.",
	"Detects the number of grues nearby.",
	"Provides protection. [Currently protection does nothing.]",
	"Teleports you to its linked structure.",
	"Teleports you to origin (0,0).",
	"Enhances the effects of its linked structure. [Does nothing.]",
	"Provides light in a large radius.",
	"Scans for crystal caches and displays the direction.",
	"An emergy glowsuit charger.",
	"Supercharges your glowsuit destroying the structure in the process.",

	"Allows the construction of intermediate structures.",
	"Allows the construction of advanced structures.",
	"Allows the construction of gather structures.",
	"Allows the construction of info structures.",
	"Allows the construction of mobile structures.",
	"Allows the construction of guard structures.",
	"Allows the construction of assault structures.",

	"A dark tower.",
}

const (
	highestPriority = iota
	highPriority
	mediumPriority
	lowPriority
	lowestPriority
)

type structureDef struct {
	longName          string
	shortName         string
	symbol            rune
	lightNeeded       int
	powerNeeded       int
	cost              int
	repairCost        int
	priority          int
	numLinks          int
	linkNames         [maxLinks]string
	covers            int
	canChargeGlowsuit bool
	ancient           bool
	newBehavior       func() structureBehavior
}

var emptyLinks = [maxLinks]string{"", "", "", "", ""}

var structureDefinitions = []structureDef{
	{"Life Structure", "Life", 'Y', 0, 0, 14, 1, lowestPriority, 0, emptyLinks, 7, true, false, newGodStructureBehavior},
	{"Power Structure", "Power", 'T', 1, 0, 8, 2, lowestPriority, 1, [maxLinks]string{"Target"}, 1, false, false, newLinkingStructureBehavior},
	{"Reactor Structure", "Reactor", '+', 1, 0, 20, 6, lowestPriority, 1, [maxLinks]string{"Target"}, 1, false, false, newLinkingStructureBehavior},
	{"Area Light Structure", "Area", 'X', 0, 0, 32, 12, lowestPriority, 0, emptyLinks, 7, true, false, newActivateStructureBehavior},
	{"Beam North Light Structure", "Beam North", 'v', 1, 0, 16, 8, lowestPriority, 0, emptyLinks, 30, true, false, newActivateStructureBehavior},
	{"Beam East Light Structure", "Beam East", '<', 1, 0, 16, 8, lowestPriority, 0, emptyLinks, 30, true, false, newActivateStructureBehavior},
	{"Beam South Light Structure", "Beam South", '^', 1, 0, 16, 8, lowestPriority, 0, emptyLinks, 30, true, false, newActivateStructureBehavior},
	{"Beam West Light Structure", "Beam West", '>', 1, 0, 16, 8, lowestPriority, 0, emptyLinks, 30, true, false, newActivateStructureBehavior},
	{"Wall Structure", "Wall", '#', 0, 0, 1, 0, lowestPriority, 0, emptyLinks, 0, false, false, newDumbStructureBehavior},
	{"Charging Structure", "Charger", 'g', 1, 2, 28, 12, lowestPriority, 0, emptyLinks, 12, false, false, newPassiveStructureBehavior},
	{"Harvesting Structure", "Harvester", '$', 1, 1, 40, 20, lowestPriority, 0, emptyLinks, 12, false, false, newPassiveStructureBehavior},
	{"Storage Structure", "Storage", '0', 1, 0, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newStorageStructureBehavior},
	{"Repair Structure", "Repairer", '`', 1, 2, 32, 24, highPriority, 3, [maxLinks]string{"Repair 1", "Repair 2", "Repair 3"}, 20, false, false, newPassiveStructureBehavior},
	{"Dim Light Structure", "Dim", '\'', 0, 0, 12, 6, lowestPriority, 0, emptyLinks, 1, false, false, newActivateStructureBehavior},
	{"Sentry Structure", "Sentry Info", '&', 1, 6, 50, 32, lowestPriority, 0, emptyLinks, 0, false, false, newLaserStructureBehavior},
	{"Time Info Structure", "Time Info", 'c', 1, 1, 12, 6, lowestPriority, 0, emptyLinks, 0, false, false, newInfoStructureBehavior},
	{"Grue Info Structure", "Grue Info", 'g', 1, 1, 12, 6, lowestPriority, 0, emptyLinks, 0, false, false, newInfoStructureBehavior},
	{"Guardian Structure", "Guardian", '&', 1, 1, 12, 6, lowestPriority, 0, emptyLinks, 20, false, false, newActivateStructureBehavior},
	{"Teleporting Structure", "Teleporter", '%', 1, 6, 50, 32, lowestPriority, 1, [maxLinks]string{"Destination"}, 200, false, false, newActionStructureBehavior},
	{"Hearth Structure", "Hearth", 'H', 1, 2, 32, 24, lowestPriority, 0, emptyLinks, 0, false, false, newActionStructureBehavior},
	{"Enhancing Structure", "Enhancer", '+', 1, 6, 64, 32, highestPriority, 1, [maxLinks]string{"Target"}, 1, false, false, newLinkingStructureBehavior},
	{"Seed Light Structure", "Seed", '.', 1, 0, 50, 24, lowestPriority, 0, emptyLinks, 10, false, false, newActivateStructureBehavior},
	{"Crystal Structure", "Crystal", 'o', 1, 6, 50, 24, lowestPriority, 0, emptyLinks, 0, false, false, newCrystalStructureBehavior},
	{"Emergency Structure", "Emergency", '!', 0, 0, 50, 50, lowestPriority, 0, emptyLinks, 0, true, false, newDumbStructureBehavior},
	{"Glowsuit Structure", "Emergency", '}', 1, 5, 12, 12, lowestPriority, 0, emptyLinks, 0, true, false, newActionStructureBehavior},

	{"Intermediate Tech Structure", "Inter Tech", 'I', 1, 1, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Advanced Tech Structure", "Advanced Tech", 'A', 1, 4, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Gather Tech Structure", "Gather Tech", 'L', 1, 2, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Info Tech Structure", "Info Tech", '?', 1, 0, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Mobile Tech Structure", "Mobile Tech", '~', 1, 3, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Guard Tech Structure", "Guard Tech", 'G', 1, 3, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},
	{"Assault Tech Structure", "Assault Tech", '~', 1, 5, 10, 5, lowestPriority, 0, emptyLinks, 0, false, false, newActivateStructureBehavior},

	{"Dark Tower Structure", "Dark Tower", '|', -1000, 0, 0, 25, lowestPriority, 0, emptyLinks, 0, false, true, newDarkStructureBehavior},
}

type structureKind int

const (
	structureLife structureKind = iota
	structurePower
	structureReactor
	structureArea
	structureBeamNorth
	structureBeamEast
	structureBeamSouth
	structureBeamWest
	structureWall
	structureCharger
	structureHarvester
	structureStorage
	structureRepairer
	structureDim
	structureSentry
	structureTimeInfo
	structureGrueInfo
	structureGuardian
	structureTeleporter
	structureHearth
	structureEnhancer
	structureSeed
	structureCrystal
	structureEmergency
	structureGlowsuit

	structureInterTech
	structureAdvancedTech
	structureGatherTech
	structureInfoTech
	structureMobileTech
	structureGuardTech
	structureAssaultTech

	structureDarkTower

	structureKindCount
)

type structureCategoryKind int

const (
	structureTier0 structureCategoryKind = iota
	structureTier1
	structureTier2
	structureGather
	structureSurvey
	structureMobile
	structureGuard
	structureAssault
	structureCategoryCount
)

func (cc structureCategoryKind) String() string {
	return structureCategories[cc].name
}

func (cc structureCategoryKind) structures() []structureKind {
	return structureCategories[cc].structures
}

type structureCategory struct {
	name       string
	structures []structureKind
}

var structureCategories = []structureCategory{
	{"Basic", []structureKind{
		structurePower,
		structureBeamEast, structureBeamNorth,
		structureBeamSouth, structureBeamWest,
		structureInterTech,
		structureGatherTech,
		structureInfoTech,
	}},
	{"Intermediate", []structureKind{
		structureArea,
		structureStorage,
		structureHearth,
		structureMobileTech,
		structureGuardTech,
		structureAdvancedTech,
	}},
	{"Advanced", []structureKind{
		structureReactor,
		structureDim,
		structureEnhancer,
		structureSeed,
		structureAssaultTech,
	}},
	{"Gather", []structureKind{
		structureHarvester,
		structureCrystal,
	}},
	{"Info", []structureKind{
		structureGrueInfo,
		structureTimeInfo,
	}},
	{"Mobile", []structureKind{
		structureTeleporter,
		structureEmergency,
		structureGlowsuit,
	}},
	{"Guard", []structureKind{
		structureWall,
		//structureGuardian,
		structureRepairer,
		structureCharger,
	}},
	{"Assault", []structureKind{
		structureSentry,
	}},
}

type structure struct {
	kind      structureKind
	id        int
	loc       point
	power     int
	lit       bool
	hampered  bool
	broken    bool
	links     [maxLinks]int
	empowered int
	behavior  structureBehavior
}

func newStructure(id int, kind structureKind, loc point) *structure {
	behavior := structureDefinitions[kind].newBehavior()

	return &structure{
		kind, id, loc, 0, false, false, false, [maxLinks]int{-1, -1, -1, -1, -1}, 0, behavior,
	}
}

func (s *structure) active() bool {
	return !s.broken && !s.hampered && s.lit && s.power >= s.def().powerNeeded
}

func (s *structure) linkedWith(id int) bool {
	def := structureDefinitions[s.kind]
	for i := 0; i < def.numLinks; i++ {
		if s.links[i] == id {
			return true
		}
	}
	return false
}

func (s *structure) slotLinked(i int) bool {
	return s.links[i] != -1
}

func (s *structure) slot(i int) int {
	return s.links[i]
}

func (s *structure) canLinkWith(other *structure) bool {
	// TODO(gruebite): Add distance check.
	return !s.linkedWith(other.id) && s.id != other.id && s.loc.distanceTo(other.loc) <= float64(s.def().covers)
}

func (s *structure) def() *structureDef {
	return &structureDefinitions[s.kind]
}

func (s *structure) powerBy(w *world, amount int) {
	past := s.active()
	s.power += amount
	now := s.active()
	if past != now {
		if s.active() {
			s.behavior.activate(w, s)
		} else {
			s.behavior.deactivate(w, s)
		}
	}
}

func (s *structure) damage(w *world, to bool) {
	past := s.active()
	s.broken = to
	now := s.active()
	if past != now {
		if s.active() {
			s.behavior.activate(w, s)
		} else {
			s.behavior.deactivate(w, s)
		}
	}
}

func (s *structure) hamper(w *world, to bool) {
	past := s.active()
	s.hampered = to
	now := s.active()
	if past != now {
		if s.active() {
			s.behavior.activate(w, s)
		} else {
			s.behavior.deactivate(w, s)
		}
	}
}
func (s *structure) light(w *world, to bool) {
	past := s.active()
	s.lit = to
	now := s.active()
	if past != now {
		if s.active() {
			s.behavior.activate(w, s)
		} else {
			s.behavior.deactivate(w, s)
		}
	}
}

func (s *structure) String() string {
	return fmt.Sprintf("%s#%d", s.def().shortName, s.id)
}

type lightLevelCheckBehavior struct{}

func (lightLevelCheckBehavior) update(w *world, s *structure) {
	if s.def().lightNeeded <= w.lightLevel(s.loc) {
		s.light(w, true)
		return
	}
	s.light(w, false)
}

type activateStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActionListBehavior
	noDoActionBehavior
	noInfoListBehavior
}

func newActivateStructureBehavior() structureBehavior {
	return &activateStructureBehavior{}
}

func (activateStructureBehavior) activate(w *world, s *structure) {
	loc := s.loc
	switch s.kind {
	case structureArea, structureDim, structureSeed:
		w.lightCircle(loc, s.def().covers, 1)
	case structureBeamNorth:
		w.lightLine(loc, loc.add(point{0, -s.def().covers}), 1)
		w.lightLine(loc.add(point{-1, 0}), loc.add(point{-1, -s.def().covers}), 1)
		w.lightLine(loc.add(point{1, 0}), loc.add(point{1, -s.def().covers}), 1)
	case structureBeamEast:
		w.lightLine(loc, loc.add(point{s.def().covers, 0}), 1)
		w.lightLine(loc.add(point{0, -1}), loc.add(point{s.def().covers, -1}), 1)
		w.lightLine(loc.add(point{0, 1}), loc.add(point{s.def().covers, 1}), 1)
	case structureBeamSouth:
		w.lightLine(loc, loc.add(point{0, s.def().covers}), 1)
		w.lightLine(loc.add(point{-1, 0}), loc.add(point{-1, s.def().covers}), 1)
		w.lightLine(loc.add(point{1, 0}), loc.add(point{1, s.def().covers}), 1)
	case structureBeamWest:
		w.lightLine(loc, loc.add(point{-s.def().covers, 0}), 1)
		w.lightLine(loc.add(point{0, -1}), loc.add(point{-s.def().covers, -1}), 1)
		w.lightLine(loc.add(point{0, 1}), loc.add(point{-s.def().covers, 1}), 1)
	case structureGuardian:
		w.protectCircle(loc, s.def().covers, 1)
	case structureInterTech:
		lvl := w.sage.unlocks[structureTier1]
		w.sage.unlocks[structureTier1] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained intermediate tech.", nil)
		}
	case structureAdvancedTech:
		lvl := w.sage.unlocks[structureTier2]
		w.sage.unlocks[structureTier2] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained advanced tech.", nil)
		}
	case structureGuardTech:
		lvl := w.sage.unlocks[structureGuard]
		w.sage.unlocks[structureGuard] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained guard tech.", nil)
		}
	case structureGatherTech:
		lvl := w.sage.unlocks[structureGather]
		w.sage.unlocks[structureGather] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained gather tech.", nil)
		}
	case structureInfoTech:
		lvl := w.sage.unlocks[structureSurvey]
		w.sage.unlocks[structureSurvey] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained info tech.", nil)
		}
	case structureMobileTech:
		lvl := w.sage.unlocks[structureMobile]
		w.sage.unlocks[structureMobile] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained mobile tech.", nil)
		}
	case structureAssaultTech:
		lvl := w.sage.unlocks[structureAssault]
		w.sage.unlocks[structureAssault] = lvl + 1
		if lvl+1 == 1 {
			w.e.Emit("message:Gained assault tech.", nil)
		}
	}
}

func (activateStructureBehavior) deactivate(w *world, s *structure) {
	loc := s.loc
	switch s.kind {
	case structureArea, structureDim, structureSeed:
		w.lightCircle(loc, s.def().covers, -1)
	case structureBeamNorth:
		w.lightLine(loc, loc.add(point{0, -s.def().covers}), -1)
		w.lightLine(loc.add(point{-1, 0}), loc.add(point{-1, -s.def().covers}), -1)
		w.lightLine(loc.add(point{1, 0}), loc.add(point{1, -s.def().covers}), -1)
	case structureBeamEast:
		w.lightLine(loc, loc.add(point{s.def().covers, 0}), -1)
		w.lightLine(loc.add(point{0, -1}), loc.add(point{s.def().covers, -1}), -1)
		w.lightLine(loc.add(point{0, 1}), loc.add(point{s.def().covers, 1}), -1)
	case structureBeamSouth:
		w.lightLine(loc, loc.add(point{0, s.def().covers}), -1)
		w.lightLine(loc.add(point{-1, 0}), loc.add(point{-1, s.def().covers}), -1)
		w.lightLine(loc.add(point{1, 0}), loc.add(point{1, s.def().covers}), -1)
	case structureBeamWest:
		w.lightLine(loc, loc.add(point{-s.def().covers, 0}), -1)
		w.lightLine(loc.add(point{0, -1}), loc.add(point{-s.def().covers, -1}), -1)
		w.lightLine(loc.add(point{0, 1}), loc.add(point{-s.def().covers, 1}), -1)
		//case structureGuardian:
		//w.protectCircle(loc, s.def().covers, -1)
	case structureInterTech:
		lvl := w.sage.unlocks[structureTier1]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureTier1] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost intermediate tech.", nil)
		}
	case structureAdvancedTech:
		lvl := w.sage.unlocks[structureTier2]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureTier2] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost advanced tech.", nil)
		}
	case structureGuardTech:
		lvl := w.sage.unlocks[structureGuard]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureGuard] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost guard tech.", nil)
		}
	case structureGatherTech:
		lvl := w.sage.unlocks[structureGather]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureGather] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost gather tech.", nil)
		}
	case structureInfoTech:
		lvl := w.sage.unlocks[structureSurvey]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureSurvey] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost info tech.", nil)
		}
	case structureMobileTech:
		lvl := w.sage.unlocks[structureMobile]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureMobile] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost mobile tech.", nil)
		}
	case structureAssaultTech:
		lvl := w.sage.unlocks[structureAssault]
		if lvl == 0 {
			panic("bug")
		}
		w.sage.unlocks[structureAssault] = lvl - 1
		if lvl-1 == 0 {
			w.e.Emit("message:Lost assault tech.", nil)
		}
	}
}

type passiveStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	noActionListBehavior
	noDoActionBehavior
	noInfoListBehavior
}

func newPassiveStructureBehavior() structureBehavior {
	return &passiveStructureBehavior{}
}

func (p *passiveStructureBehavior) update(w *world, s *structure) {
	p.lightLevelCheckBehavior.update(w, s)
	if !s.active() {
		return
	}
	switch s.kind {
	case structureCharger:
		if w.sage.loc.distanceTo(s.loc) >= float64(s.def().covers) {
			return
		}
		if w.sage.glowsuit <= sageGlowsuitMaxPower-2 {
			w.sage.glowsuit += 2
		}
	case structureHarvester:
		radius := s.def().covers
		for dx := -radius; dx <= radius; dx++ {
			for dy := -radius; dy <= radius; dy++ {
				if dx*dx+dy*dy >= radius*radius {
					continue
				}
				spot, exists := w.crystalSpots[point{dx, dy}]
				if exists {
					if spot.crystals > 0 {
						spot.lifetime = crystalLifetime
					} else {
						spot.lifetime = (spot.lifetime * 9) / 10
					}
				}
			}
		}
	case structureRepairer:
		for l := 0; l < s.def().numLinks; l++ {
			if !s.slotLinked(l) {
				continue
			}
			id := s.slot(l)
			s2 := w.findStructureByID(id)
			if s2.broken {
				s2.damage(w, false)
			}
		}
	}
}

type dumbStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	noActionListBehavior
	noDoActionBehavior
	noInfoListBehavior
}

func newDumbStructureBehavior() structureBehavior {
	return &dumbStructureBehavior{}
}

type storageStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	stored int
}

func newStorageStructureBehavior() structureBehavior {
	return &storageStructureBehavior{}
}

func (b *storageStructureBehavior) actionList(s *structure) []string {
	switch s.kind {
	case structureStorage:
		return []string{fmt.Sprintf("Store 25 crystals."), fmt.Sprintf("Take 25 crystals.")}
	}
	return nil
}

func (b *storageStructureBehavior) doAction(w *world, s *structure, i int) {
	switch s.kind {
	case structureStorage:
		if i == 0 {
			if w.sage.crystals < 25 {
				w.e.Emit("message:Not enough crystals to store!", nil)
			} else {
				w.sage.crystals -= 25
				b.stored = 25
				w.e.Emit("message:Stored 25 crystals!", nil)
			}
		} else {
			if b.stored != 25 {
				w.e.Emit("message:Not enough stored crystals!", nil)
			} else {
				w.sage.crystals = min(sageMaxCrystals, w.sage.crystals+25)
				b.stored = 0
				w.e.Emit("message:Took 25 crystals!", nil)
			}
		}
	}
}

func (b *storageStructureBehavior) infoList(w *world, s *structure) []string {
	switch s.kind {
	case structureStorage:
		return []string{fmt.Sprintf("Stored: %d", b.stored)}
	}
	return nil
}

type actionStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	noInfoListBehavior
}

func newActionStructureBehavior() structureBehavior {
	return &actionStructureBehavior{}
}

func (actionStructureBehavior) actionList(s *structure) []string {
	switch s.kind {
	case structureTeleporter:
		return []string{fmt.Sprintf("Teleport to linked.")}
	case structureHearth:
		return []string{fmt.Sprintf("Teleport to origin.")}
	case structureGlowsuit:
		return []string{fmt.Sprintf("Supercharge glowsuit.")}
	}
	return nil
}

func (actionStructureBehavior) doAction(w *world, s *structure, i int) {
	switch s.kind {
	case structureHearth:
		if !s.active() {
			w.e.Emit("message:Teleport failed. Structure inactive.", nil)
			return
		}
		w.teleportSage(point{0, 0})
		w.e.Emit(fmt.Sprintf("message:Successfully teleported to origin!"), nil)
	case structureTeleporter:
		if !s.active() {
			w.e.Emit("message:Teleport failed. Structure inactive.", nil)
			return
		}
		if i != 0 {
			break
		}
		if !s.slotLinked(0) {
			w.e.Emit("message:Teleport failed. No structures linked.", nil)
			break
		}
		s2 := w.findStructureByID(s.slot(0))
		if s2 == nil {
			w.e.Emit("message:Teleport failed. Linked structure no longer exists.", nil)
			break
		}
		w.teleportSage(s2.loc)
		w.e.Emit(fmt.Sprintf("message:Successfully teleported to %v!", s2), nil)
	case structureGlowsuit:
		w.e.Emit("message:Glowsuit supercharged!", nil)
		w.sage.glowsuit = sageGlowsuitMaxPower * 3
		w.destroyStructureAt(s.loc)
	}
}

type laserStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	noActionListBehavior
	noDoActionBehavior
	cooldown int
	target   point
}

func newLaserStructureBehavior() structureBehavior {
	return &laserStructureBehavior{}
}

func (b *laserStructureBehavior) update(w *world, s *structure) {
	b.lightLevelCheckBehavior.update(w, s)
	if !s.active() {
		return
	}
	if b.cooldown == 60 {
		w.lightLine(s.loc, b.target, -100)
	}
	if b.cooldown > 0 {
		b.cooldown = max(b.cooldown-1, 0)
		return
	}
	if s.loc.distanceTo(w.sage.loc) > float64(grueSpawnRadius*2+s.def().covers) {
		return
	}
	if w.grues.gruePopulationOnMap() <= 0 {
		return
	}
	grues := w.grues.gruesNearestTo(s.loc)
	grue := grues[0]
	if s.loc.distanceTo(grue.loc) >= float64(s.def().covers) {
		return
	}
	b.target = grue.loc
	w.lightLine(s.loc, grue.loc, 1000)
	b.cooldown = 60
}

func (b *laserStructureBehavior) infoList(w *world, s *structure) []string {
	if b.cooldown <= 0 {
		return []string{"Cooldown: [Ready]"}
	}
	return []string{fmt.Sprintf("Cooldown: %d", b.cooldown)}
}

type infoStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	noDoActionBehavior
	noActionListBehavior
}

func newInfoStructureBehavior() structureBehavior {
	return &infoStructureBehavior{}
}

func (infoStructureBehavior) infoList(w *world, s *structure) []string {
	if !s.active() {
		return nil
	}
	switch s.kind {
	case structureTimeInfo:
		return []string{
			fmt.Sprintf("Time passed: %v (%f)", w.turnCount, calcScale(w.turnCount, w.diff)),
		}
	case structureGrueInfo:
		return []string{
			fmt.Sprintf("Grue population: %v", w.grues.gruePopulationOnMap()),
		}
	}
	return nil
}

type godStructureBehavior struct {
	lightLevelCheckBehavior
	noLinkBehavior
	noUnlinkBehavior
	noDoActionBehavior
	noActionListBehavior
}

func newGodStructureBehavior() structureBehavior {
	return &godStructureBehavior{}
}

func (b *godStructureBehavior) activate(w *world, s *structure) {
	loc := s.loc
	switch s.kind {
	case structureLife:
		w.lightCircle(loc, s.def().covers, 1)
	}
}

func (b *godStructureBehavior) deactivate(w *world, s *structure) {
	loc := s.loc
	switch s.kind {
	case structureLife:
		w.lightCircle(loc, s.def().covers, -1)
	}
}

func (b *godStructureBehavior) infoList(w *world, s *structure) []string {
	switch s.kind {
	case structureLife:
		return []string{
			"This is the last structure. You need to",
			"build more to survive. Collect crystals,",
			"build up a base, and avoid [grues]--you",
			"can't kill them yet.",
			"",
			"Some structures need power provided by",
			"[Power Structures]. If a structure is",
			"dark, it's not functioning.",
			"",
			"Arrows to move, space to build.",
			"",
			"Goodluck.",
		}
	}
	return nil
}

type linkingStructureBehavior struct {
	lightLevelCheckBehavior
	noDoActionBehavior
	noActionListBehavior
	noInfoListBehavior
}

func newLinkingStructureBehavior() structureBehavior {
	return &linkingStructureBehavior{}
}

func (b *linkingStructureBehavior) activate(w *world, s *structure) {
	ss := w.sortedStructuresByDistance(s.loc)[1:]
	switch s.kind {
	case structurePower:
		for id := 0; id < s.def().numLinks; id++ {
			if !s.slotLinked(id) {
				for len(ss) > 0 {
					if s.canLinkWith(ss[0]) && !s.linkedWith(ss[0].id) {
						s.links[id] = ss[0].id
						break
					}
					ss = ss[1:]
				}
			}
			if !s.slotLinked(id) {
				continue
			}
			sid := s.slot(id)
			s := w.findStructureByID(sid)
			if s == nil {
				continue
			}
			s.powerBy(w, 1)
		}
	case structureReactor:
		for id := 0; id < s.def().numLinks; id++ {
			if !s.slotLinked(id) {
				for len(ss) > 0 {
					if s.canLinkWith(ss[0]) && !s.linkedWith(ss[0].id) {
						s.links[id] = ss[0].id
						break
					}
					ss = ss[1:]
				}
			}
			if !s.slotLinked(id) {
				continue
			}
			sid := s.slot(id)
			s := w.findStructureByID(sid)
			if s == nil {
				continue
			}
			s.powerBy(w, 3)
		}
	}
}

func (b *linkingStructureBehavior) deactivate(w *world, s *structure) {
	switch s.kind {
	case structurePower:
		for slot := 0; slot < s.def().numLinks; slot++ {
			if !s.slotLinked(slot) {
				continue
			}
			sid := s.slot(slot)
			s := w.findStructureByID(sid)
			if s == nil {
				continue
			}
			s.powerBy(w, -1)
		}
	case structureReactor:
		for slot := 0; slot < s.def().numLinks; slot++ {
			if !s.slotLinked(slot) {
				continue
			}
			sid := s.slot(slot)
			s := w.findStructureByID(sid)
			if s == nil {
				continue
			}
			s.powerBy(w, -3)
		}
	}
}

func (b *linkingStructureBehavior) link(w *world, s *structure, sid int) {
	switch s.kind {
	case structurePower:
		s2 := w.findStructureByID(sid)
		if s2 == nil {
			return
		}
		s2.powerBy(w, 1)
	case structureReactor:
		s2 := w.findStructureByID(sid)
		if s2 == nil {
			return
		}
		s2.powerBy(w, 3)
	}
}

func (b *linkingStructureBehavior) unlink(w *world, s *structure, slot int) {
	switch s.kind {
	case structurePower:
		id := s.slot(slot)
		s := w.findStructureByID(id)
		if s == nil {
			return
		}
		s.powerBy(w, -1)
	case structureReactor:
		id := s.slot(slot)
		s := w.findStructureByID(id)
		if s == nil {
			return
		}
		s.powerBy(w, -3)
	}
}

type darkStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noInfoListBehavior
	initialized bool
	delta       point
}

func newDarkStructureBehavior() structureBehavior {
	return &darkStructureBehavior{}
}

func (b *darkStructureBehavior) activate(w *world, s *structure) {
	if !b.initialized {
		b.initialized = true
		b.delta = w.sage.loc.add(s.loc.neg()).mul(2)
	}
	loc := s.loc
	switch s.kind {
	case structureDarkTower:
		var side point
		if b.delta.x == 0 {
			side = point{1, 0}
		} else {
			side = point{0, 1}
		}
		w.lightLine(loc, loc.add(b.delta), -500)

		w.lightLine(loc.add(side.mul(1)), loc.add(b.delta).add(side.mul(1)), -500)
		w.lightLine(loc.add(side.mul(-1)), loc.add(b.delta).add(side.mul(-1)), -500)

		w.lightLine(loc.add(side.mul(2)), loc.add(b.delta).add(side.mul(2)), -500)
		w.lightLine(loc.add(side.mul(-2)), loc.add(b.delta).add(side.mul(-2)), -500)

	}
}

func (b *darkStructureBehavior) deactivate(w *world, s *structure) {
	loc := s.loc
	switch s.kind {
	case structureDarkTower:
		var side point
		if b.delta.x == 0 {
			side = point{1, 0}
		} else {
			side = point{0, 1}
		}
		w.lightLine(loc, loc.add(b.delta), 500)

		w.lightLine(loc.add(side.mul(1)), loc.add(b.delta).add(side.mul(1)), 500)
		w.lightLine(loc.add(side.mul(-1)), loc.add(b.delta).add(side.mul(-1)), 500)

		w.lightLine(loc.add(side.mul(2)), loc.add(b.delta).add(side.mul(2)), 500)
		w.lightLine(loc.add(side.mul(-2)), loc.add(b.delta).add(side.mul(-2)), 500)
	}
}

func (b *darkStructureBehavior) doAction(w *world, s *structure, i int) {
	switch s.kind {
	case structureDarkTower:
		w.sage.glowsuit = 0
		w.destroyStructureAt(s.loc)
	}
}

func (b *darkStructureBehavior) actionList(s *structure) []string {
	switch s.kind {
	case structureDarkTower:
		return []string{"Destroy (depletes glowsuit!)"}
	}
	return nil
}

type crystalStructureBehavior struct {
	noLinkBehavior
	noUnlinkBehavior
	lightLevelCheckBehavior
	noActivateBehavior
	noDeactivateBehavior
	loc *point
}

func newCrystalStructureBehavior() structureBehavior {
	return &crystalStructureBehavior{}
}

func (b *crystalStructureBehavior) doAction(w *world, s *structure, i int) {
	switch s.kind {
	case structureCrystal:
		if b.loc != nil {
			w.crystalSpots[*b.loc].lifetime = 0
		}
		mx := 1
		if w.rng.Float64() < 0.5 {
			mx = -1
		}
		my := 1
		if w.rng.Float64() < 0.5 {
			my = -1
		}
		dx := roll(w.rng, 50, 100) * mx
		dy := roll(w.rng, 50, 100) * my
		spot := s.loc.add(point{dx, dy})
		if !w.isLit(spot) {
			w.e.Emit("message:Located a crystal cluster.", nil)
			_, exists := w.crystalSpots[spot]
			if !exists {
				w.crystalSpots[spot] = &crystalSpot{}
			}
			w.crystalSpots[spot].lifetime = crystalLifetime
			w.crystalSpots[spot].crystals = roll(w.rng, crystalMin, crystalMax*24)
			b.loc = &spot
		} else {
			w.e.Emit("message:Could not locate a crystal cluster.", nil)
			b.loc = nil
		}
	}
}

func (b *crystalStructureBehavior) actionList(s *structure) []string {
	switch s.kind {
	case structureCrystal:
		return []string{"Scan for crystals."}
	}
	return nil
}

func (b *crystalStructureBehavior) infoList(w *world, s *structure) []string {
	switch s.kind {
	case structureCrystal:
		if b.loc == nil {
			return []string{"No crystal clusters nearby."}
		}
		return []string{fmt.Sprintf("Scan result: %s", b.loc.add(s.loc.neg()).cardinal())}
	}
	return nil
}

type structureSystem struct {
}
