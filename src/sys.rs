
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};

use specs::*;
use bear_lib_terminal::{terminal, Color};

use global::*;
use comp::*;

pub struct LightMap(pub HashMap<Position, i32>);
impl Deref for LightMap {
    type Target = HashMap<Position, i32>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for LightMap {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct StructureMap(pub HashMap<Position, Entity>);
impl Deref for StructureMap {
    type Target = HashMap<Position, Entity>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl DerefMut for StructureMap {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Player(pub Entity);

pub struct StructureSystem {
    inserted_id: ReaderId<InsertedFlag>,
    removed_id: ReaderId<RemovedFlag>,
    inserted: BitSet,
    removed: BitSet,
    saved_keys: HashMap<u32, Position>
}
impl StructureSystem {
    pub fn new(world: &mut World) -> StructureSystem {
        let mut sts = world.write_storage::<Structure>();
        StructureSystem {
            inserted_id: sts.track_inserted(),
            removed_id: sts.track_removed(),
            inserted: BitSet::new(),
            removed: BitSet::new(),
            saved_keys: HashMap::new(),
        }
    }
}
impl<'a> System<'a> for StructureSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, LightMap>,
        WriteExpect<'a, StructureMap>,
        ReadStorage<'a, Structure>,
        ReadStorage<'a, Position>,
        ReadExpect<'a, LazyUpdate>
    );

    fn run(&mut self, (entities, lightmap, mut structuremap, structs, poss, updater): Self::SystemData) {
        self.inserted.clear();
        self.removed.clear();
        structs.populate_inserted(&mut self.inserted_id, &mut self.inserted);
        structs.populate_removed(&mut self.removed_id, &mut self.removed);

        let sm = &mut **structuremap;

        // Important to do removed first in case id was reused.
        for id in (&self.removed).join() {
            if let Some(pos) = self.saved_keys.remove(&id) {
                sm.remove(&pos);
            }
        }

        for (ent, _, pos, id) in (&*entities, &structs, &poss, &self.inserted).join() {
            sm.insert(*pos, ent);
            self.saved_keys.insert(id, *pos);
        }

        let lm = &**lightmap;

        for (ent, pos, st) in (&*entities, &poss, &structs).join() {
            if st.light_required <= *lm.get(pos).unwrap_or(&0) {
                updater.insert(ent, Active);
                updater.insert(ent, Lit);
            } else {
                updater.remove::<Lit>(ent);
                updater.remove::<Active>(ent);
            }
        }
    }
}

pub struct LightSystem {
    inserted_active: ReaderId<InsertedFlag>,
    removed_active: ReaderId<RemovedFlag>,
    inserted: BitSet,
    removed: BitSet,
    saved_pos: HashMap<u32, Position>,
    saved_ls: HashMap<u32, LightSource>
}
impl LightSystem {
    pub fn new(world: &mut World) -> LightSystem {
        let iact = world.write_storage::<Active>().track_inserted();
        let ract = world.write_storage::<Active>().track_removed();
        LightSystem {
            removed_active: ract,
            inserted_active: iact,
            inserted: BitSet::new(),
            removed: BitSet::new(),
            saved_pos: HashMap::new(),
            saved_ls: HashMap::new()
        }
    }
}
impl<'a> System<'a> for LightSystem {
    type SystemData = (
        WriteExpect<'a, LightMap>,
        ReadStorage<'a, LightSource>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Active>
    );

    fn run(&mut self, (mut lightmap, lss, poss, acts): Self::SystemData) {
        self.inserted.clear();
        self.removed.clear();
        acts.populate_inserted(&mut self.inserted_active, &mut self.inserted);
        acts.populate_removed(&mut self.removed_active, &mut self.removed);

        let lm = &mut **lightmap;

        // Important to do removed first in case id was reused.
        for id in (&self.removed).join() {
            let pos = self.saved_pos.remove(&id);
            let ls = self.saved_ls.remove(&id);
            // This is possible when a LightSource was added and removed in the same frame.
            if pos.is_none() || ls.is_none() {
                continue;
            }
            let pos = pos.unwrap();
            let ls = ls.unwrap();
            for x in -ls.radius..ls.radius+1 {
                for y in -ls.radius..ls.radius+1 {
                    if x * x + y * y < ls.radius * ls.radius {
                        let p = Position::new(pos.x + x, pos.y + y);
                        *lm.get_mut(&p).unwrap() -= ls.amount;
                        if *lm.get(&p).unwrap() == 0 {
                            lm.remove(&p);
                        }
                    }
                }
            }
        }

        for (ls, pos, id) in (&lss, &poss, &self.inserted).join() {
            self.saved_pos.insert(id, *pos);
            self.saved_ls.insert(id, *ls);
            for x in -ls.radius..ls.radius+1 {
                for y in -ls.radius..ls.radius+1 {
                    if x * x + y * y < ls.radius * ls.radius {
                        let p = Position::new(pos.x + x, pos.y + y);
                        *lm.entry(p).or_insert(0) += ls.amount;
                    }
                }
            }
        }
    }
}

pub struct RenderSystem;
impl<'a> System<'a> for RenderSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, Player>,
        ReadExpect<'a, LightMap>,
        ReadStorage<'a, Glyph>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Lit>
    );

    fn run(&mut self, (entities, player, lightmap, glyphs, poss, lits): Self::SystemData) {
        let Player(sage) = *player;
        let sage_pos = poss.get(sage).unwrap();

        let white = Color::from_rgb(255, 255, 255);
        let black = Color::from_rgb(0, 0, 0);

        for (pos, &level) in &**lightmap {
            let x = pos.x - sage_pos.x + FIELD_WIDTH / 2;
            let y = pos.y - sage_pos.y + FIELD_HEIGHT / 2;
            if x >= 0 && x < FIELD_WIDTH && y >= 0 && y < FIELD_HEIGHT && level > 0 {
                terminal::set_colors(black, white);
                terminal::put_xy(x, y, ' ');
            }
        }

        for (ent, glyph, pos) in (&*entities, &glyphs, &poss).join() {
            let x = pos.x - sage_pos.x + FIELD_WIDTH / 2;
            let y = pos.y - sage_pos.y + FIELD_HEIGHT / 2;
            if x >= 0 && x < FIELD_WIDTH && y >= 0 && y < FIELD_HEIGHT {
                let is_lit = lits.get(ent).is_some();
                if is_lit {
                    terminal::set_colors(black, white);
                } else {
                    terminal::set_colors(white, black);
                }
                terminal::put_xy(x, y, glyph.id);
            }
        }
    }
}

pub struct GlowsuitSystem;
impl<'a> System<'a> for GlowsuitSystem {
    type SystemData = (
        Entities<'a>,
        ReadExpect<'a, LightMap>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Glowsuit>,
        WriteStorage<'a, Lit>
    );

    fn run(&mut self, (entities, lightmap, poss, mut glowsuits, mut lits): Self::SystemData) {
        let lm = &**lightmap;
        for (ent, pos, glowsuit) in (&*entities, &poss, &mut glowsuits).join() {
            let in_light = *lm.get(pos).unwrap_or(&0) > 0;
            if in_light {
                lits.insert(ent, Lit).unwrap();
            } else {
                glowsuit.power = *[0, glowsuit.power - 1].iter().max().unwrap();
                if glowsuit.power == 0 {
                    lits.remove(ent);
                }
            }
        }
    }
}