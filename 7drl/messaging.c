
#include <stdbool.h>

#include "messaging.h"
#include "vector.h"

static bool was_init = false;
static struct vector messages;
static int max_length = -1;

void messaging_init_or_die(int max_length);
void messaging_start_turn(void);
void messaging_push(const char *fmt, ...);
