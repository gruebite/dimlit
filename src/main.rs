
extern crate bear_lib_terminal;
extern crate shrev;
extern crate specs;

use std::collections::HashMap;

use specs::*;
use bear_lib_terminal::{
    terminal,
    Color
};

mod act;
#[macro_use]
mod dialog;
mod util;
mod global;
mod comp;
mod sys;

use act::*;
use dialog::*;
use global::*;
use comp::*;
use sys::*;

#[derive(Clone, Copy)]
enum EscapeAction {
    Quit
}

#[derive(Clone, Copy)]
enum DialogAction {
    Escape(EscapeAction),
    Cancel
}

fn render_status(world: &mut World, sage: Entity) {
    terminal::set_colors(Color::from_rgb(255, 255, 255), Color::from_rgb(0, 0, 0));
    terminal::print_xy(FIELD_WIDTH, 0, "Dimlit");
    let mut positions: WriteStorage<Position> = world.system_data();
    let sage_pos = positions.get_mut(sage).unwrap();
    terminal::print_xy(FIELD_WIDTH, 1, &format!("Position: {}", sage_pos));
    let glowsuits: WriteStorage<Glowsuit> = world.system_data();
    let glowsuit = glowsuits.get(sage).unwrap();
    terminal::print_xy(FIELD_WIDTH, 2, &format!("Glowsuit: {}", glowsuit.power));
}

fn render_dialog<T>(dialog: &DialogFrame<T>) {
    let mut y = 0;
    for i in 0..dialog.item_count() {
        let (item, sel) = dialog.item(i);
        if sel {
            terminal::set_colors(Color::from_rgb(0, 0, 0), Color::from_rgb(255, 255, 255));
        } else {
            terminal::set_colors(Color::from_rgb(255, 255, 255), Color::from_rgb(0, 0, 0));
        }
        terminal::print_xy(FIELD_WIDTH, y, item.text().as_ref());
        if item.has_subdialog() {
            terminal::print_xy(FIELD_WIDTH+39, y, ">");
        }
        y += 1;
    }
}

fn create_escape_dialog() -> Dialog<DialogAction> {
    let frame = frame!(
        ("Dimlit", false, None, None),
        ("-----", false, None, None),
        ("Quit", true, Some(DialogAction::Escape(EscapeAction::Quit)), None),
        ("Cancel", true, Some(DialogAction::Cancel), None)
    );
    /*
    let mut frame = DialogFrame::new();
    frame.add_item("Dimlit", false, None, None);
    frame.add_item("-----", false, None, None);
    frame.add_item("Quit", true, Some(DialogAction::Escape(EscapeAction::Quit)), None);
    frame.add_item("Cancel", true, Some(DialogAction::Cancel), None);
    */
    Dialog::new(frame)
}

fn main() {
    terminal::open("Dimlit", 80, 25);

    let mut world = World::new();
    world.register::<Active>();
    world.register::<Glyph>();
    world.register::<Glowsuit>();
    world.register::<Position>();
    world.register::<LightSource>();
    world.register::<Lit>();
    world.register::<Structure>();

    let mut dispatcher = DispatcherBuilder::new()
        .with(RenderSystem, "render", &[])
        .with(LightSystem::new(&mut world), "light", &[])
        .with(StructureSystem::new(&mut world), "structure", &[])
        .build();

    let mut turn_dispatcher = DispatcherBuilder::new()
        .with(GlowsuitSystem, "glowsuit", &[])
        .build();

    let sage = world.create_entity()
        .with(Glyph { id: '@' })
        .with(Position { x: 0, y: 0 })
        .with(Glowsuit { power: 100 })
        .with(Lit)
        .build();
    world.add_resource(Player(sage));
    world.add_resource(LightMap(HashMap::new()));
    world.add_resource(StructureMap(HashMap::new()));

    let structure = world.create_entity()
        .with(Glyph { id: '&' })
        .with(Position { x: 0, y: 1 })
        .with(LightSource { amount: 1, radius: 5 })
        .with(Structure { light_required: 0, power_required: 0 })
        .with(Active)
        .build();

    let mut dialog = Dialog::new(DialogFrame::new());
    dialog.deactivate();

    loop {
        terminal::set_colors(Color::from_rgb(255, 255, 255), Color::from_rgb(0, 0, 0));
        terminal::clear(None);
        if let Some(frame) = dialog.current_frame() {
            render_dialog(frame);
        } else {
            render_status(&mut world, sage);
        }
        dispatcher.dispatch(&world.res);
        world.maintain();
        terminal::refresh();

        let maybe_event = terminal::read_event();
        if maybe_event.is_none() {
            continue;
        }

        let event = maybe_event.unwrap();
        let act: Action = event.into();
        if dialog.is_active() {
            let res = dialog.process(act);
            match res {
                Some(DialogAction::Escape(EscapeAction::Quit)) => {
                    break;
                }
                Some(DialogAction::Cancel) => dialog.deactivate(),
                _ => {}
            }
        } else {
            let mut moved = false;
            let mut sage_pos = *world.system_data::<WriteStorage<Position>>().get_mut(sage).unwrap();
            match act {
                Action::Escape => {
                    dialog = create_escape_dialog();
                }
                Action::Left => {
                    let sm = &**world.system_data::<ReadExpect<StructureMap>>();
                    if let None = sm.get(&Position::new(sage_pos.x - 1, sage_pos.y)) {
                        sage_pos.x -= 1;
                        moved = true;
                    }
                }
                Action::Right => {
                    sage_pos.x += 1;
                    moved = true;
                }
                Action::Up => {
                    sage_pos.y -= 1;
                    moved = true;
                }
                Action::Down => {
                    sage_pos.y += 1;
                    moved = true;
                }
                Action::Enact => {
                    world.delete_entity(structure).unwrap();
                }
                _ => {}
            }

            if moved {
                *world.system_data::<WriteStorage<Position>>().get_mut(sage).unwrap() = sage_pos;
                turn_dispatcher.dispatch(&world.res);
                world.maintain();
            }
        }
    }

    terminal::close();
}