/*

TODO

* Add vector ref, replace get/set transactions
* Add crystal_cmp avoid confusion
* Messages
- Add grues killing in the dark, possibly a turn module
- Finish menus, structure commands, hero commands
- Glow suit
- More structures
- Main menu, survival mode
- More towers
- Refactor (clean up), functional, need to know information
- Events
- Sounds
- Generalize dialog (callbacks? return enum?)
- Save/load
- Other modes

BUGS

- Crystals can spawn on structures
- Structures are inefficient to find by location, problem?

NOTES

- Create systems when they materialize
- Focus on limiting information to functions and returning something useful (referential transparency)
- Top functions should do the modifying
- Compose everything in one spot, sage reference to world, break up world into structures which are built?
- Refactor!

*/
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <termbox.h>

#include "terminal.h"
#include "vector.h"

#define VIEW_X 0
#define VIEW_Y 0
#define VIEW_WIDTH 40
#define VIEW_HEIGHT 20
#define VIEW_CENTER_X (VIEW_X + VIEW_WIDTH / 2)
#define VIEW_CENTER_Y (VIEW_Y + VIEW_HEIGHT / 2)

#define MESSAGE_X 0
#define MESSAGE_Y 21
#define MESSAGE_WIDTH 40
#define MESSAGE_HEIGHT 3

#define STATUS_X 40
#define STATUS_Y 0
#define STATUS_WIDTH 40
#define STATUS_HEIGHT 24

#define MAX_LINKS 5
#define CRYSTAL_LIFETIME 25
#define CRYSTAL_SPAWN_CHANCE 2

struct point {
        int x;
        int y;
};

static int point_cmp(const void *a, const void *b)
{
        const struct point *ap = a;
        const struct point *bp = b;
        return ap->x == bp->x ? ap->y - bp->y : ap->x - bp->x;
}

static bool point_within_range(struct point a, struct point b, int range)
{
        int dx = a.x - b.x;
        int dy = a.y - b.y;
        return dx * dx + dy * dy < range * range;
}

enum structure_type {
        STRUCT_NONE,
        STRUCT_AREA
};

static const struct structure_def {
        const char *name;
        char symbol;
        int cost;
        int priority;
        int num_links;
        int max_crystals;
        int power_consumption;
} STRUCTURES[] = {
        {"NULL", '?', 0, 0, 0, 0, 0},
        {"Light Area Structure", '&', 100, 1, 0, 1000, 5},
};

struct structure {
        enum structure_type type;
        int id;
        struct point loc;
        bool activated;
        bool hampered;
        int links[MAX_LINKS];
        int empowered;
        int crystals; 
};

static int structure_cmp(const void *a, const void *b)
{
        const struct structure *as = a;
        const struct structure *bs = b;
        return STRUCTURES[as->type].priority - STRUCTURES[bs->type].priority;
}

struct crystal {
        /* Point should be the first member. This is used for sorting. */
        struct point loc;
        int crystals;
        int lifetime;
};

static int crystal_cmp(const void *a, const void *b)
{
        /* Yes. Points. A point should be the first member of a crystal. */
        const struct point *ap = a;
        const struct point *bp = b;
        return ap->x == bp->x ? ap->y - bp->y : ap->x - bp->x;
}

struct world {
        struct point sage_loc;
        struct vector structures;
        struct vector lit;
        struct vector crystals;
        struct vector messages;
        int messages_added;
        int turn_count;
        int sage_crystals;
        int sage_sight;
};

enum move_type {
        MOVE_NONE,
        MOVE_SUCCESS,
        MOVE_BUMP_STRUCTURE,
        MOVE_DO_ACTION
};

struct move_result {
        enum move_type type;
        struct point loc;
};

struct dialog {
        struct dialog *parent;
        bool exists_and_allocd;
        int selected;
        struct vector items;
};

struct dialog_item {
        bool selectable;
        char text[STATUS_WIDTH];
        struct dialog subdialog;
};

enum action {
        ACTION_NONE,
        ACTION_LEFT,
        ACTION_DOWN,
        ACTION_UP,
        ACTION_RIGHT,
        ACTION_BACK,
        ACTION_ACTION
};

static enum action convert_key_to_action(struct tb_event *ev);

static void clear_rect(int x, int y, int w, int h);
static void clear_status(void);

static int world_init(struct world *world);
static void world_uninit(struct world *world);
static void world_update(struct world *world);
static void world_display(struct world *world);
static int world_add_message(struct world *world, const char *fmt, ...);
static int world_add_structure(struct world *world, enum structure_type type, struct point loc);
static bool world_is_lit(struct world *world, struct point at);
static struct move_result world_move_sage(struct world *world, int dx, int dy);

static int dialog_init(struct dialog *d);
static void dialog_uninit(struct dialog *d);
static void dialog_select_first_item(struct dialog *d);
static void dialog_select_next_item(struct dialog *d);
static void dialog_select_prev_item(struct dialog *d);
static int dialog_build_structure(struct dialog *d, struct world *world, struct point loc);
static void dialog_display(struct dialog *d);
static void dialog_process_input(struct dialog *d, const char **selected, struct dialog **next);

int main(void)
{
        srand(time(NULL));

        struct world world;
        world_init(&world);

        struct dialog dialog = {0};
        struct dialog *selected_dialog = NULL;

        tb_init();
        atexit(tb_shutdown);

        while (true) {
                tb_clear();
                world_display(&world);
                if (selected_dialog) {
                        clear_status();
                        dialog_display(selected_dialog);
                }
                tb_present();

                const char *_unused;
                if (selected_dialog) {
                        dialog_process_input(selected_dialog, &_unused, &selected_dialog);
                        continue;
                }

                struct move_result result = {MOVE_NONE};
                struct tb_event ev;
                tb_poll_event(&ev);
                enum action action = convert_key_to_action(&ev);
                switch (action) {
                case ACTION_LEFT:
                        result = world_move_sage(&world, -1, 0);
                        break;
                case ACTION_DOWN:
                        result = world_move_sage(&world, 0, 1);
                        break;
                case ACTION_UP:
                        result = world_move_sage(&world, 0, -1);
                        break;
                case ACTION_RIGHT:
                        result = world_move_sage(&world, 1, 0);
                        break;
                case ACTION_ACTION:
                        result = (struct move_result){MOVE_DO_ACTION};
                        break;
                case ACTION_BACK:
                        goto exit_loop;
                case ACTION_NONE:
                        break;
                }

                if (result.type == MOVE_NONE) {
                        continue;
                }
                if (result.type == MOVE_BUMP_STRUCTURE) {
                        dialog_build_structure(&dialog, &world, result.loc);
                        selected_dialog = &dialog;
                } else {
                        world_update(&world);
                }
        }
exit_loop:

        dialog_uninit(&dialog);
        world_uninit(&world);
        return 0;
}

static enum action convert_key_to_action(struct tb_event *ev)
{
        switch (ev->ch) {
        case 'h':
                return ACTION_LEFT;
        case 'j':
                return ACTION_DOWN;
        case 'k':
                return ACTION_UP;
        case 'l':
                return ACTION_RIGHT;
        case ' ':
                return ACTION_ACTION;
        case 'Q':
                return ACTION_BACK;
        }

        switch (ev->key) {
        case TB_KEY_ARROW_LEFT:
                return ACTION_LEFT;
        case TB_KEY_ARROW_DOWN:
                return ACTION_DOWN;
        case TB_KEY_ARROW_UP:
                return ACTION_UP;
        case TB_KEY_ARROW_RIGHT:
                return ACTION_RIGHT;
        case TB_KEY_ESC:
                return ACTION_BACK;
        }

        return ACTION_NONE;
}

static void clear_rect(int x, int y, int w, int h)
{
        for (int xx = x; xx < x + w; ++xx) {
                for (int yy = y; yy < y + h; ++yy) {
                        tb_change_cell(xx, yy, ' ', TB_DEFAULT, TB_DEFAULT);
                }
        }
}

static void clear_status(void)
{
        clear_rect(STATUS_X, STATUS_Y, STATUS_WIDTH, STATUS_HEIGHT);
}

int world_init(struct world *world)
{
        memset(world, 0, sizeof(*world));
        world->sage_loc.x = 0;
        world->sage_loc.y = 0;
        world->sage_crystals = 0;
        world->sage_sight = 5;
        if (vector_init(&world->structures, 8, sizeof(struct structure)) != 0) {
                world_uninit(world);
                return -1;
        }
        if (vector_init(&world->lit, 128, sizeof(struct point)) != 0) {
                world_uninit(world);
                return -1;
        }
        if (vector_init(&world->crystals, 128, sizeof(struct crystal)) != 0) {
                world_uninit(world);
                return -1;
        }
        if (vector_init(&world->messages, 128, MESSAGE_WIDTH) != 0) {
                world_uninit(world);
                return -1;
        }
        int id = world_add_structure(world, STRUCT_AREA, (struct point){0, 1});
        if (id < 0) {
                world_uninit(world);
                return -1;
        }
        struct structure *s;
        vector_ref(&world->structures, id, (void *)&s);
        s->crystals = 100 + STRUCTURES[s->type].power_consumption;
        world_update(world);
        return 0;
}

void world_uninit(struct world *world)
{
        vector_uninit(&world->messages);
        vector_uninit(&world->crystals);
        vector_uninit(&world->lit);
        vector_uninit(&world->structures);
}

void world_update(struct world *world)
{
        world->messages_added = 0;

        world->lit.length = 0;
        for (int i = 0; i < world->structures.length; ++i) {
                struct structure *s;
                vector_ref(&world->structures, i, (void *)&s);
                if (s->crystals < STRUCTURES[s->type].power_consumption) {
                        continue;
                }
                s->crystals -= STRUCTURES[s->type].power_consumption;
                for (int dx = -5; dx <= 5; ++dx) {
                        for (int dy = -5; dy <= 5; ++dy) {
                                if (dx * dx + dy * dy >= 25) {
                                        continue;
                                }
                                struct point p = {s->loc.x + dx, s->loc.y + dy};
                                vector_ordered_insert(&world->lit, &p, point_cmp);
                        }
                }
        }

        for (int i = world->crystals.length - 1; i >= 0; --i) {
                struct crystal *c;
                vector_ref(&world->crystals, i, (void *)&c);
                if (c->lifetime-- <= 0) {
                        vector_del(&world->crystals, i, NULL);
                }
        }

        for (int dx = -world->sage_sight; dx < world->sage_sight; ++dx) {
                for (int dy = -world->sage_sight; dy < world->sage_sight; ++dy) {
                        if (dx * dx + dy * dy >= world->sage_sight * world->sage_sight) {
                                continue;
                        }
                        struct point p = {world->sage_loc.x + dx, world->sage_loc.y + dy};
                        if (vector_bsearch(&world->crystals, &p, crystal_cmp, NULL) >= 0) {
                                continue;
                        }
                        struct crystal c = {p, 0, CRYSTAL_LIFETIME};
                        if (rand() % 100 < CRYSTAL_SPAWN_CHANCE) {
                                c.crystals = rand() % 9 + 1;
                        }
                        vector_ordered_insert(&world->crystals, &c, crystal_cmp);
                }
        }

        if (world->messages_added == 0) {
                world_add_message(world, "%d", rand());
        }

        world->turn_count++;
}

void world_display(struct world *world)
{
        int view_x = world->sage_loc.x - VIEW_WIDTH / 2;
        int view_y = world->sage_loc.y - VIEW_HEIGHT / 2;

#define _WITHIN(x, y, xx, yy, w, h) \
        ((x) >= (xx) && (x) < ((xx) + (w)) && (y) >= (yy) && (y) < ((yy) + (h)))

        /* Draw lit. */
        for (int i = 0; i < world->lit.length; ++i) {
                struct point p;
                vector_get(&world->lit, i, &p);
                if (!_WITHIN(p.x, p.y, view_x, view_y, VIEW_WIDTH, VIEW_HEIGHT)) {
                        continue;
                }
                tb_change_cell(p.x - view_x, p.y - view_y, ' ', TB_DEFAULT | TB_REVERSE, TB_DEFAULT);
        }

        /* Draw structures. */
        for (int i = 0; i < world->structures.length; ++i) {
                struct structure s;
                vector_get(&world->structures, i, &s);
                if (!_WITHIN(s.loc.x, s.loc.y, view_x, view_y, VIEW_WIDTH, VIEW_HEIGHT)) {
                        continue;
                }
                tb_change_cell(s.loc.x - view_x, s.loc.y - view_y, '&', TB_DEFAULT | TB_REVERSE, TB_DEFAULT);
        }

        for (int i = 0; i < world->crystals.length; ++i) {
                struct crystal c;
                vector_get(&world->crystals, i, &c);
                if (!point_within_range(c.loc, world->sage_loc, world->sage_sight)) {
                        continue;
                }
                if (!_WITHIN(c.loc.x, c.loc.y, view_x, view_y, VIEW_WIDTH, VIEW_HEIGHT)) {
                        continue;
                }
                if (c.crystals <= 0) {
                        continue;
                }
                if (world_is_lit(world, c.loc)) {
                        continue;
                }
                tb_change_cell(c.loc.x - view_x, c.loc.y - view_y, '*', TB_DEFAULT, TB_DEFAULT);
        }

        /* Draw sage. */
        int fg = TB_DEFAULT;
        bool is_lit = world_is_lit(world, world->sage_loc);
        if (is_lit) {
                fg |= TB_REVERSE;
        }
        tb_change_cell(VIEW_CENTER_X, VIEW_CENTER_Y, '@', fg, TB_DEFAULT);

        for (int i = world->messages.length - 1; i >= 0; --i) {
                int delta = (world->messages.length - 1) - i;
                if (delta >= MESSAGE_HEIGHT) {
                        break;
                }
                int y = MESSAGE_Y + MESSAGE_HEIGHT - 1 - delta;
                const char *msg;
                vector_ref(&world->messages, i, (void *)&msg);
                printf_tb(MESSAGE_X, y, TB_DEFAULT, TB_DEFAULT, msg);
        }

        /* Draw statuses. */
        printf_tb(STATUS_X, STATUS_Y, TB_DEFAULT, TB_DEFAULT, "Dimlit");
        printf_tb(STATUS_X, STATUS_Y + 1, TB_DEFAULT, TB_DEFAULT, "Crystals: %d", world->sage_crystals);
        printf_tb(STATUS_X, STATUS_Y + 2, TB_DEFAULT, TB_DEFAULT, "Turn: %d", world->turn_count);
        printf_tb(STATUS_X, STATUS_Y + 3, TB_DEFAULT, TB_DEFAULT, "Coord: %d, %d", world->sage_loc.x, world->sage_loc.y);
        printf_tb(STATUS_X, STATUS_Y + 4, TB_DEFAULT, TB_DEFAULT, is_lit ? "Lit!" : "Darkness...");
}

int world_add_message(struct world *world, const char *fmt, ...)
{
        world->messages_added++;
        char buf[MESSAGE_WIDTH];
	va_list vl;
	va_start(vl, fmt);
	vsnprintf(buf, sizeof(buf), fmt, vl);
	va_end(vl);
        return vector_push_back(&world->messages, buf);
}

int world_add_structure(struct world *world, enum structure_type type, struct point loc)
{
        int id = -1;
        for (int i = 0; i < world->structures.length; ++i) {
                struct structure s;
                vector_get(&world->structures, i, &s);
                if (s.type == STRUCT_NONE) {
                        id = i;
                        break;
                }
        }

        if (id == -1) {
                id = world->structures.length;
        }

        struct structure s = {
                type, id, loc, true, false, {-1, -1, -1, -1, -1}, 0, 0
        };

        if (vector_ordered_insert(&world->structures, &s, structure_cmp) != 0) {
                return -1;
        }

        return id;
}

bool world_is_lit(struct world *world, struct point at)
{
        return vector_bsearch(&world->lit, &at, point_cmp, NULL) >= 0;
}

struct move_result world_move_sage(struct world *world, int dx, int dy)
{
        int x = world->sage_loc.x + dx;
        int y = world->sage_loc.y + dy;
        struct move_result result = {.type = MOVE_SUCCESS};
        for (int i = 0; i < world->structures.length; ++i) {
                struct structure s;
                vector_get(&world->structures, i, &s);
                if (s.loc.x == x && s.loc.y == y) {
                        result.type = MOVE_BUMP_STRUCTURE;
                        result.loc = (struct point){x, y};
                        return result;
                }
        }
        world->sage_loc = (struct point){x, y};
        if (!world_is_lit(world, world->sage_loc)) {
                int i = vector_bsearch(&world->crystals, &world->sage_loc, crystal_cmp, NULL);
                struct crystal c;
                vector_del(&world->crystals, i, &c);
                world->sage_crystals += c.crystals;
        }
        return result;
}

static int dialog_init(struct dialog *d)
{
        d->exists_and_allocd = true;
        d->selected = -1;
        return vector_init(&d->items, 8, sizeof(struct dialog_item));
}

static void dialog_uninit(struct dialog *d)
{
        if (!d->exists_and_allocd) {
                return;
        }
        for (int i = 0; i < d->items.length; ++i) {
                struct dialog_item item;
                vector_get(&d->items, i, &item);
                dialog_uninit(&item.subdialog);
        }
        vector_uninit(&d->items);
}

static void dialog_select_first_item(struct dialog *d)
{
        for (int i = 0; i < d->items.length; ++i) {
                struct dialog_item item;
                vector_get(&d->items, i, &item);
                if (item.selectable) {
                        d->selected = i;
                        break;
                }
        }
}

static void dialog_select_next_item(struct dialog *d)
{
        if (d->selected == -1) {
                return;
        }
        int i = (d->selected + 1) % d->items.length;
        while (i != d->selected) {
                struct dialog_item item;
                vector_get(&d->items, i, &item);
                if (item.selectable) {
                        d->selected = i;
                        break;
                }
                i = (i + 1) % d->items.length;
        }
}

static void dialog_select_prev_item(struct dialog *d)
{
        if (d->selected == -1) {
                return;
        }
        int i = (d->selected - 1) % d->items.length;
        while (i != d->selected) {
                struct dialog_item item;
                vector_get(&d->items, i, &item);
                if (item.selectable) {
                        d->selected = i;
                        break;
                }
                i--;
                if (i < 0) {
                        i = d->items.length;
                }
        }
}

static int dialog_build_structure(struct dialog *d, struct world *world, struct point loc)
{
        struct structure s;
        bool found = false;
        for (int i = 0; i < world->structures.length; ++i) {
                vector_get(&world->structures, i, &s);
                if (s.loc.x == loc.x && s.loc.y == loc.y) {
                        found = true;
                        break;
                }
        }

        if (!found) {
                return -1;
        }

        if (d->exists_and_allocd) {
                dialog_uninit(d);
        }
        if (dialog_init(d) != 0) {
                return -1;
        }

        struct dialog_item item;

        snprintf(item.text, STATUS_WIDTH, "%s — #%d", STRUCTURES[s.type].name, s.id);
        item.selectable = false;
        vector_push_back(&d->items, &item);

        snprintf(item.text, STATUS_WIDTH, "%s %d/%d",
                s.activated ? "Active" : "Inactive",
                s.crystals, STRUCTURES[s.type].max_crystals);
        item.selectable = false;
        vector_push_back(&d->items, &item);

        for (int i = 0; i < STATUS_WIDTH; ++i) {
                item.text[i] = '-';
                if (i == STATUS_WIDTH - 1) {
                        item.text[i] = '\0';
                }
        }
        item.selectable = false;
        vector_push_back(&d->items, &item);

        dialog_select_first_item(d);

        return 0;
}

static void dialog_display(struct dialog *d)
{
        int y = STATUS_Y;
        for (int i = 0; i < d->items.length; ++i) {
                struct dialog_item item;
                vector_get(&d->items, i, &item);
                int fg = d->selected == i ? TB_REVERSE : 0;
                printf_tb(STATUS_X, y, TB_DEFAULT | fg, TB_DEFAULT, item.text);
                y++;
        }
}

static void dialog_process_input(struct dialog *d, const char **selected, struct dialog **next)
{
        struct tb_event ev;
        tb_poll_event(&ev);

        struct dialog_item item;

        enum action action = convert_key_to_action(&ev);

        switch (action) {
        case ACTION_LEFT: case ACTION_BACK:
                *next = d->parent;
                break;
        case ACTION_DOWN:
                dialog_select_next_item(d);
                break;
        case ACTION_UP:
                dialog_select_prev_item(d);
                break;
        default:
                break;
        }
}
