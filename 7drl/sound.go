// +build !linux
// +build amd64

package main

import (
	"math/rand"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	"github.com/olebedev/emitter"
)

type world struct {
	rng          *rand.Rand
	diff         difficultyLevel
	sage         sage
	nextID       int
	structures   map[point]*structure
	lit          map[point]int
	protected    map[point]int
	crystalSpots map[point]*crystalSpot
	darkness     *darkness
	messages     []string
	messageAdded bool
	turnCount    int
	e            *emitter.Emitter
	messaging    *messagingSystem
	sound        *soundSystem
	grues        *grues
}

func newWorld() *world {
	w := new(world)
	w.rng = rand.New(rand.NewSource(time.Now().UnixNano()))
	w.sage.sight = sageSightRange
	w.sage.glowsuit = sageGlowsuitMaxPower
	w.sage.speed = 1
	w.sage.unlocks = make(map[structureCategoryKind]int)
	w.sage.unlocks[structureTier0] = 1
	w.structures = make(map[point]*structure)
	w.lit = make(map[point]int)
	w.protected = make(map[point]int)
	w.crystalSpots = make(map[point]*crystalSpot)
	w.crystalSpots[point{0, -6}] = &crystalSpot{16, 0, crystalLifetime}
	w.darkness = newDarkness()
	w.messages = make([]string, 0)
	w.addStructure(structureLife, point{0, 1})
	w.turnCount = 0
	w.e = &emitter.Emitter{}
	w.e.Use("*", emitter.Void)
	w.messaging = newMessaging(w.e, &w.sage)
	w.sound = newSoundSystem(w.e)
	w.grues = newGrues(w.e)
	w.update()
	return w
}

type soundSystem struct {
	darkCtrl  *beep.Ctrl
	lightCtrl *beep.Ctrl
	explosion beep.StreamSeekCloser
}

func newSoundSystem(e *emitter.Emitter) *soundSystem {
	f1, _ := os.Open("data/darkness.wav")
	s1, format, _ := wav.Decode(f1)
	f2, _ := os.Open("data/structure.wav")
	s2, format, _ := wav.Decode(f2)
	f3, _ := os.Open("data/explosion.wav")
	s3, format, _ := wav.Decode(f3)

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	darkCtrl := &beep.Ctrl{Streamer: beep.Loop(-1, s1)}
	darkCtrl.Paused = true
	speaker.Play(darkCtrl)
	lightCtrl := &beep.Ctrl{Streamer: beep.Loop(-1, s2)}
	lightCtrl.Paused = true
	speaker.Play(lightCtrl)

	e.On("sound:*", func(event *emitter.Event) {
		switch event.OriginalTopic {
		case "sound:dark":
			darkCtrl.Paused = false
			lightCtrl.Paused = true
		case "sound:light":
			darkCtrl.Paused = true
			lightCtrl.Paused = false
		case "sound:explosion":
			speaker.Play(s3)
		case "sound:stopAll":
			darkCtrl.Paused = true
			lightCtrl.Paused = true
		}
	})

	return &soundSystem{darkCtrl, lightCtrl, s3}
}
