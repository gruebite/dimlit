package dialog

type dialogItem struct {
	text       string
	selectable bool
	returns    interface{}
	subdialog  *Dialog
}

type DialogAction int

const (
	DialogNoAction DialogAction = iota
	DialogSelect
	DialogNext
	DialogPrev
	DialogBack
	DialogLeave
)

type DialogReturnsExit struct{}

// Dialog is a menu. It keeps track of selections and subdialogs.
type Dialog struct {
	parent   *Dialog
	curr     *Dialog
	selected int
	items    []dialogItem
}

func New() *Dialog {
	return &Dialog{
		nil,
		nil,
		-1,
		make([]dialogItem, 0),
	}
}

func (d *Dialog) AddItem(text string, selectable bool, returns interface{}, subdialog *Dialog) {
	if subdialog != nil {
		subdialog.parent = d
		subdialog.curr = nil
	}
	d.items = append(d.items, dialogItem{
		text,
		selectable,
		returns,
		subdialog,
	})
}

func (d *Dialog) selectFirstItem() {
	d.selected = -1
	for i := 0; i < len(d.items); i++ {
		if d.items[i].selectable {
			d.selected = i
			break
		}
	}
}

func (d *Dialog) Activate() {
	d.curr = d
	d.curr.selectFirstItem()
}

func (d *Dialog) Active() bool {
	return d.curr != nil
}

func (d *Dialog) selectNextItem() {
	if d.selected == -1 {
		return
	}
	i := (d.selected + 1) % len(d.items)
	for i != d.selected {
		if d.items[i].selectable {
			d.selected = i
			break
		}
		i = (i + 1) % len(d.items)
	}
}

func (d *Dialog) selectPrevItem() {
	if d.selected == -1 {
		return
	}
	i := d.selected - 1
	if i < 0 {
		i = len(d.items) - 1
	}
	for i != d.selected {
		if d.items[i].selectable {
			d.selected = i
			break
		}

		i--
		if i < 0 {
			i = len(d.items) - 1
		}
	}
}

func (d *Dialog) PerformAction(action DialogAction) interface{} {
	if d.curr == nil {
		panic("inactive dialog")
	}

	switch action {
	case DialogSelect:
		if d.curr.selected != -1 {
			item := d.curr.items[d.curr.selected]
			if item.subdialog != nil {
				d.curr = item.subdialog
				d.curr.selectFirstItem()
			} else {
				d.curr = nil
				return item.returns
			}
		}
	case DialogNext:
		d.curr.selectNextItem()
	case DialogPrev:
		d.curr.selectPrevItem()
	case DialogBack:
		if d.curr != nil {
			d.curr = d.curr.parent
		}
	case DialogLeave:
		d.curr = nil
	}
	return nil
}

func (d *Dialog) Length() int {
	if d.curr == nil {
		panic("inactive dialog")
	}
	return len(d.curr.items)
}

func (d *Dialog) Item(i int) (string, bool) {
	if d.curr == nil {
		panic("inactive dialog")
	}
	return d.curr.items[i].text, i == d.curr.selected
}

func (d *Dialog) ItemHasSubdialog(i int) bool {
	if d.curr == nil {
		panic("inactive dialog")
	}
	return d.curr.items[i].subdialog != nil
}

func (d *Dialog) ItemIsSelectable(i int) bool {
	if d.curr == nil {
		panic("inactive dialog")
	}
	return d.curr.items[i].selectable
}

func (d *Dialog) ItemReturns(i int) interface{} {
	if d.curr == nil {
		panic("inactive dialog")
	}
	return d.curr.items[i].returns
}
