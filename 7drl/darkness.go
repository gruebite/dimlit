package main

import (
	"fmt"
	"math/rand"
)

type darknessEventDef struct {
	chance  float64
	factory func(rng *rand.Rand) darknessEvent
}

var darknessEventDefinitions = []darknessEventDef{
	{0.0005, newDarkSurge},
	{0.001, newDarkStorm},
	{0.0005, newDarkShatter},
	{0.0005, newDarkLightning},
	{0.0005, newDarkTower},
}

type darknessEventKinds int

const (
	darknessEventSurge darknessEventKinds = iota
	darknessEventStorm
	darknessEventShatter
	darknessEventLightning
	darknessEventTower
	darknessEventKindsCount
)

type darknessEvent interface {
	update(w *world)
	stillGoing() bool
}

type darkness struct {
	darknessEvents []darknessEvent
}

func newDarkness() *darkness {
	return &darkness{
		[]darknessEvent{},
	}
}

func (d *darkness) update(w *world) {
	if w.turnCount < gracePeriod {
		return
	}
	for i := 0; i < int(darknessEventKindsCount); i++ {
		de := darknessEventDefinitions[i]
		if w.rng.Float64() < de.chance {
			d.darknessEvents = append(d.darknessEvents, de.factory(w.rng))
		}
	}

	for _, d := range d.darknessEvents {
		d.update(w)
	}

	for i := len(d.darknessEvents) - 1; i >= 0; i-- {
		if !d.darknessEvents[i].stillGoing() {
			d.darknessEvents = append(d.darknessEvents[:i], d.darknessEvents[i+1:]...)
		}
	}
}

type darkSurge struct{}

func newDarkSurge(rng *rand.Rand) darknessEvent {
	return &darkSurge{}
}

func (d *darkSurge) update(w *world) {
	w.flashView()
	w.e.Emit("message:There was a surge of darkness.", nil)
	w.damageSage(roll(w.rng, 5, 50))
}

func (d *darkSurge) stillGoing() bool {
	return false
}

type darkStorm struct {
	initialized        bool
	affectedStructures map[int]bool
	duration           int
}

func newDarkStorm(rng *rand.Rand) darknessEvent {
	return &darkStorm{}
}

func (d *darkStorm) update(w *world) {
	if !d.initialized {
		d.initialized = true
		ns := max(1, int(calcScale(w.turnCount, w.diff)*0.3))
		ns = min(ns, len(w.structures))
		affects := make(map[int]bool)
		structs := w.structureList()
		i := 0
		for i < len(structs) && ns > 0 {
			if structs[i].hampered || structs[i].def().ancient {
				i++
				continue
			}
			affects[structs[i].id] = true
			w.structures[structs[i].loc].hamper(w, true)
			i++
			ns--
		}
		d.affectedStructures = affects
		w.e.Emit(fmt.Sprintf("message:A dark storm has started! %d structures are hampered.", len(affects)), nil)
		minDur := int(calcScale(w.turnCount, w.diff) * 0.1)
		maxDur := int(calcScale(w.turnCount, w.diff) * 5)
		if minDur == maxDur {
			maxDur++
		}
		d.duration = max(1, roll(w.rng, minDur, maxDur))
		return
	}
	d.duration--
	if d.duration > 0 {
		return
	}

	for id := range d.affectedStructures {
		s := w.findStructureByID(id)
		s.hamper(w, false)
	}
	w.e.Emit(fmt.Sprintf("message:A dark storm has subsided."), nil)

}

func (d *darkStorm) stillGoing() bool {
	return d.duration > 0
}

type darkShatter struct{}

func newDarkShatter(rng *rand.Rand) darknessEvent {
	return &darkShatter{}
}

func (d *darkShatter) update(w *world) {
	w.e.Emit(fmt.Sprintf("message:Heavy darkness has shattered all materialized crystals."), nil)
	for _, c := range w.crystalSpots {
		c.crystals = 0
		c.lifetime = crystalLifetime
	}
}

func (d *darkShatter) stillGoing() bool {
	return false
}

type darkLightning struct{}

func newDarkLightning(rng *rand.Rand) darknessEvent {
	return &darkLightning{}
}

func (d *darkLightning) update(w *world) {
	w.e.Emit(fmt.Sprintf("message:You hear a loud crack from the dark. A structure was destroyed."), nil)
	w.e.Emit("sound:explosion")

	keys := make([]point, 0)
	for loc, s := range w.structures {
		if !s.def().ancient {
			keys = append(keys, loc)
		}
	}
	key := keys[w.rng.Intn(len(keys))]
	w.destroyStructureAt(key)
}

func (d *darkLightning) stillGoing() bool {
	return false
}

type darkTower struct{}

func newDarkTower(rng *rand.Rand) darknessEvent {
	return &darkTower{}
}

func (d *darkTower) update(w *world) {
	w.e.Emit(fmt.Sprintf("message:The grues have constructed a dark tower. Find it and destroy it."), nil)

	const darkTowerDistanceMin = 30
	const darkTowerDistanceMax = 50
	dist := roll(w.rng, darkTowerDistanceMin, darkTowerDistanceMax)
	dirs := []point{
		point{0, -1},
		point{0, 1},
		point{-1, 0},
		point{1, 0},
	}
	dir := dirs[w.rng.Intn(4)]
	i := 1
	for {
		check := dir.mul(i)
		if !w.isLit(check) {
			dist--
			if dist <= 0 {
				break
			}
		}
		i++
	}
	w.addStructure(structureDarkTower, w.sage.loc.add(dir.mul(i)))
}

func (d *darkTower) stillGoing() bool {
	return false
}
