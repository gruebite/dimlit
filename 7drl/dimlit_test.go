package main

import (
	"fmt"
	"testing"
)

func TestEverything(t *testing.T) {
	{
		es1 := newEnergyStore(50)
		es2 := newBlackHoleEnergyStore()

		fmt.Println(es1.send(&es2, 30, false).centicrystals())
		fmt.Println(es1, es2)
	}
	{
		es1 := newEnergyStore(50)
		es2 := newBlackHoleEnergyStore()

		fmt.Println(es1.send(&es2, 51, false).centicrystals())
		fmt.Println(es1, es2)
	}
	{
		es1 := newEnergyStore(50)
		es2 := newBlackHoleEnergyStore()

		fmt.Println(es1.send(&es2, -50, false).centicrystals())
		fmt.Println(es1, es2)
	}
	{
		es1 := newEnergyStore(3000)
		es1.energy = 0
		es2 := newEnergyStore(2304)

		fmt.Println(es1.recieve(&es2, 2304, true).centicrystals())
		fmt.Println(es1, es2)
	}
}
