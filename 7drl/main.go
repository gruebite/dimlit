/*

                            |
Dimlit                   --###--
                *         +###+
 *                        /###\   *
        *                 #####
                         +#####+
     *          *  @     /#####\
____,,,,;;;;;;;######################;;


TODO

Core

* Messages
* Add grues killing in the dark, possibly a turn module
	* Death
* Finish menus, structure commands, hero commands
* Glow suit
- More structures, mining, transports, teleports, distributors, beam lights, enhancers
	* Linking
	* Beam
	* Mining
	* Transports
	* Teleports
	- Distributors
	- Enhancers // uses linkTo interface to increase/decrease enchancement
	+ Information towers instead of big statuses
	* Scan tower for crystals and grues
	* Laser tower
	- Unique structures (can only have one)
	- Sign structures
	* Teleport to origin
	- Lightrail (fast moving)
- Improve structure/behavior dependencies
* Sentry structure that adds "protected" spots like light does
* Represent light as numbers and only remove 1 light when a tower is no longer functioning, add 1 light on activation
- Information/flavor text
- Main menu, survival mode
+ Events, ur-grues! darkness surges/storms
	* Too little light events for pushing forward "The darkness is encroaching. Build more light."
	* Some towers require minimum light to run
	- Ur-grues
* Generalize dialog (callbacks? return enum?)
* Provide a way to do a super exit dialogs
* Centicrystals
* Add linking to structure behavior interface
+ Improve darkness event system — warnings, reminders, continuous updates, better architecture for turns and ranges
- Add surprise, some stuff randomly generated at certain coordinates — maybe if you reach X,Y in octant 5 it spawns something
+ Grue tech trees, structure attacker/destroyer, player attacker (urgrue)
	+ Two grue tracks. After 5000 age eligable for urgrue. Warning every 1000 turns
* Reverse structure status active like: Powered, On, Broken, Hampered
* Damage can kill the sage, warn after 1 turn of darkness
* Better printing: Something in [brackets] is reversed
* Name link slots by having them on the def()
* Structures need one less tier of light to run then what they need to build
* Find a way to control tuning transfers. Transfer excess to chest. Low priority transfer?
+ Events to better communicate stuff. Darkness system emits "surge incoming", message system hears it
	+ Use Emitter
* Link UI draws lines to link structure by returning draw operations
- Attack/defense rhythm? Grue storms?
* Add running which burns through glowsuit energy
- Make towers function only in light?
- Information towers set flags if in range, which show stats
* Walls to protect key structures in darkness
* Autorepair, called CheckActivate structures
- Grue's drop teeth which can be used to upgrade the sage or build structures
* Create energyStorage type that handles transactions with maximums
+ Darkness system and hampering towers/pushing player
	- Pushing player
* Structures require X amount of light to be built/function
* Light structures can only be built in light? Late game light structures don't
* Subdialogs of links
* Glowsuit can only be charged at light structures
- Dark structures. They increase the frequency of events and potency.
- Harvesters are more effective the further from a light structure they are
* Extractors reduce lifetime of crystalSpots
+ Crystals lifetimes increase A LOT to encourage branching out and building crystalSpots
+ Fun choices and surprise. What choices?
* Artifacts. Far out are artifacts to find which allow you to open up specific structures. Every artefact is very far. You can build detectors.
* Shift focus from building light to upgrade, to building light to find artifacts to upgrade.
- Artifacts have story.
* Artifacts unlock tiers or specialty structures.
- https://dev.to/hajimehoshi/go-packages-we-developed-for-our-games--4cl9
* It is pitch black. With a period.
* Transferring from structure rounds up.
* Grue attacking player!
	- Also an extra, rarer grue that is super fast.
	- And another rarer grue that goes after light producing structures only.
	- And another that's an urgrue that is larger.
* Charge glowsuit represents in energy.
- Grues spawn in a recently lit area (scary)
- Artifact quantrant
- Light unearths artifacts
- Artifacts are used to upgrade
- Ancient structures on the side of the map
	- Super powerful laser
- Dark events use a randomEncounter system for weather structures
- Ancient structure locator
- Add artifact costs to structures. You can find them after shining light. They build really powerful structures. You bump into them.
* Hamper hits nearest N structures to sage
* Sort by distance so you can hamper non-hampered
- Dark structures make super dark spots
- More grues
- Artifacts
* Powers only charge nearby
- Better randomness
- Guardian structure protects you from ur-grue and crystals

Balance. This comes with more play-testing, which I didn't do as much of since I was busy adding content. This is something I want to focus more on.
Artifacts. They can only be revealed by light. Some additional tech and structures would require these.
Randomly generated structures or bases in the dark you can find. Some of these structures are unique and can only be found.
Cleverer random event grouping, and more of them.
More advanced grue AI, and more types of grues.
More structures and tech trees.
Light Rail Structure — the sage moves fast when walking on light provided by this structure.
Tablet Structure — you can write your own notes, or find ones with lore
Forecasting Structure — can detect when dark events will happen with some degree of accuracy
Grue's drop teeth that the Sage can use for upgrades.
Different modes: Journey (how far can you go) and Sandbox (focused on exploration).
Sound on all the systems. Currently only works on Mac, so if you have a Mac I recommend playing on that. I think it adds a lot to the game.

Polish

- Sounds
- Save/load
- Other modes
- Show info on structures when building
- Blinking crystals: *+*+

BUGS

* Crystals can spawn on structures
- Structures are inefficient to find by location, problem?
- Dark towers are inactive and can't be killed
- They only shoot one beam
- Too hard

NOTES

- Balance searching for artifacts with preparing.
- Create systems when they materialize
- Focus on limiting information to functions and returning something useful (referential transparency)
- Top functions should do the modifying
- Compose everything in one spot, sage reference to world, break up world into structures which are built?
- Refactor!
- Communicate changes with events, world manages events, no cross system dependency
	- World just holds systems and information
	- One system could be just lighting which book keeps where light is

Grues spawn darkness and dumb. They combine. If no darkness to spawn, there is a offloaded map. Too many grues = urgrue.
Offloaded grues can combine, just like grues on map.
Two light structures: infrastructure and big light ones. Grues have light thresholds.

*/
package main

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"

	"github.com/nsf/termbox-go"
	"gitlab.com/gruebite/dimlit/dialog"
)

const viewX = 0
const viewY = 0
const viewWidth = 40
const viewHeight = 20
const viewCenterX = viewX + viewWidth/2
const viewCenterY = viewY + viewHeight/2

const messageX = 0
const messageY = 21
const messageWidth = 80
const messageHeight = 3

const statusX = 40
const statusY = 0
const statusWidth = 40
const statusHeight = 21

type structureActionKind int

const (
	structureDoNothing structureActionKind = iota
	structureRepair
	structureChargeGlowsuit
	structureFlip
	structureGiveCrystals
	structureTakeCrystals
	structureLink
	structureDeconstruct
	structureCustomAction
)

type structureAction struct {
	kind structureActionKind
	loc  point
	arg  int
	arg2 int
}

type sageActionKind int

const (
	sageDoNothing sageActionKind = iota
	sageBuildStructure
	sageToggleRunning
	sageGiveCrystals
)

type sageAction struct {
	kind sageActionKind
	loc  point
	arg  int
}

type quitAction int

const (
	quitYes quitAction = iota
	quitNo
)

type crystalSpot struct {
	crystals int
	teeth    int
	lifetime int
}

type sageUpgrade int

const (
	sageUpgradeGlowsuit sageUpgrade = iota
	sageUpgradeRunning
	sageUpgradeStorage
)

type sage struct {
	dead             bool
	loc              point
	crystals         int
	glowsuit         int
	glowsuitCooldown int
	sight            int
	turnsInDarkness  int
	bumpedThisTurn   bool
	runningDir       point
	running          bool
	speed            int
	upgrades         map[sageUpgrade]int
	unlocks          map[structureCategoryKind]int
}

func (s *sage) canHear(from point) bool {
	return s.loc.add(from.neg()).mag2() < sageHearingRange*sageHearingRange
}

type darknessSystem struct{}
type lightnessSystem struct{}
type crystalSystem struct{}
type grueSystem struct{}
type timeSystem struct{}
type dimWorld interface {
	rng() *rand.Rand
	sage() *sage
	lightness() *lightnessSystem
	darkness() *darknessSystem
	crystals() *crystalSystem
	structures() *structureSystem
	time() *timeSystem
	grues() *grueSystem
}

func (w *world) findNearestDark(loc point) point {
	searched := make(map[point]bool)
	candidates := make([]point, 64)
	candidates = append(candidates, loc)
	for {
		var can point
		can, candidates = candidates[0], candidates[1:]
		if !w.isLit(can) {
			return can
		}
		searched[can] = true
		for _, d := range directions8 {
			n := can.add(d)
			if _, exists := searched[n]; exists {
				continue
			}
			candidates = append(candidates, n)
		}
	}
}

func (w *world) findNearestStructure(loc point) *structure {
	searched := make(map[point]bool)
	candidates := make([]point, 64)
	candidates = append(candidates, loc)
	for {
		var can point
		can, candidates = candidates[0], candidates[1:]
		if w.structureAt(can) != nil {
			return w.structureAt(can)
		}
		searched[can] = true
		for _, d := range directions8 {
			n := can.add(d)
			if _, exists := searched[n]; exists {
				continue
			}
			candidates = append(candidates, n)
		}
	}
}

func (w *world) lightCount() int {
	return len(w.lit)
}

func (w *world) isLit(loc point) bool {
	c, exists := w.lit[loc]
	return exists && c > 0
}

func (w *world) lightLevel(loc point) int {
	level, _ := w.lit[loc]
	return level
}

func (w *world) isProtected(loc point) bool {
	_, exists := w.protected[loc]
	return exists
}

func (w *world) protectPoint(loc point, by int) {
	if by == 0 {
		panic("cannot protect by 0")
	}
	if by > 0 {
		_, exists := w.protected[loc]
		if !exists {
			w.protected[loc] = by
		} else {
			w.protected[loc] += by
		}
	} else {
		_, exists := w.protected[loc]
		if exists {
			w.protected[loc] += by
			if w.protected[loc] <= 0 {
				delete(w.protected, loc)
			}
		}
	}
}

func (w *world) protectCircle(origin point, radius, by int) {
	for dx := -radius; dx <= radius; dx++ {
		for dy := -radius; dy <= radius; dy++ {
			if dx*dx+dy*dy >= radius*radius {
				continue
			}
			loc := origin.add(point{dx, dy})
			w.protectPoint(loc, by)
		}
	}
}

func (w *world) lightPoint(loc point, by int) {
	if by == 0 {
		panic("cannot light by 0")
	}
	if by > 0 {
		_, exists := w.lit[loc]
		if !exists {
			w.lit[loc] = by
		} else {
			w.lit[loc] += by
			if w.lit[loc] == 0 {
				delete(w.lit, loc)
			}
		}
	} else {
		_, exists := w.lit[loc]
		if exists {
			w.lit[loc] += by
			if w.lit[loc] == 0 {
				delete(w.lit, loc)
			}
		} else {
			w.lit[loc] += by
		}
	}
}

func (w *world) lightCircle(origin point, radius, by int) {
	for dx := -radius; dx <= radius; dx++ {
		for dy := -radius; dy <= radius; dy++ {
			if dx*dx+dy*dy >= radius*radius {
				continue
			}
			loc := origin.add(point{dx, dy})
			w.lightPoint(loc, by)
		}
	}
}

func (w *world) lightLine(origin, dest point, by int) {
	line := origin.lineTo(dest)

	for _, p := range line {
		w.lightPoint(p, by)
	}
}

func (w *world) update() {
	w.sage.bumpedThisTurn = false

	priorityStructures := make([]*structure, 0, len(w.structures))
	for _, s := range w.structures {
		priorityStructures = append(priorityStructures, s)
	}
	sort.Slice(priorityStructures, func(i, j int) bool {
		defs := structureDefinitions
		return defs[priorityStructures[i].kind].priority < defs[priorityStructures[j].kind].priority
	})

	for _, s := range priorityStructures {
		s.behavior.update(w, s)
	}

	for loc, c := range w.crystalSpots {
		c.lifetime--
		if c.lifetime <= 0 {
			delete(w.crystalSpots, loc)
			continue
		}
		if w.isLit(loc) {
			c.crystals = 0
		}
	}

	for dx := -w.sage.sight; dx <= w.sage.sight; dx++ {
		for dy := -w.sage.sight; dy <= w.sage.sight; dy++ {
			if dx*dx+dy*dy >= w.sage.sight*w.sage.sight {
				continue
			}
			test := w.sage.loc.add(point{dx, dy})
			if _, exists := w.crystalSpots[test]; exists {
				continue
			}
			if _, exists := w.structures[test]; exists {
				continue
			}
			newc := crystalSpot{0, 0, crystalLifetime}
			if w.rng.Float64() < crystalSpawnChance {
				newc.crystals = roll(w.rng, crystalMin, crystalMax)
			}
			w.crystalSpots[test] = &newc
		}
	}

	if !w.isLit(w.sage.loc) {
		if w.sage.glowsuit > 0 {
			w.sage.glowsuit = max(0, w.sage.glowsuit-1)
			if w.sage.glowsuit == sageGlowsuitWarn {
				w.e.Emit("message:Your glowsuit is getting low on power.", nil)
			}
		} else {
			w.sage.turnsInDarkness++
			if w.sage.turnsInDarkness == grueKillsAfter {
				w.e.Emit("message:You've been eaten. Press escape to exit...", nil)
				w.sage.dead = true
			} else if w.sage.turnsInDarkness == grueWarnsAfter {
				w.e.Emit("message:It is pitch black. You are likely to be eaten by a grue.", nil)
			}
		}
		w.e.Emit("sound:dark")
	} else {
		w.sage.turnsInDarkness = 0
		w.e.Emit("sound:light")
	}
	w.sage.glowsuitCooldown--

	w.darkness.update(w)
	w.grues.update(w)

	w.turnCount++
}

func (w *world) flashView() {
	cells := termbox.CellBuffer()
	width, height := termbox.Size()
	for x := viewX; x < viewX+viewWidth; x++ {
		for y := viewY; y < viewY+viewHeight; y++ {
			if x < 0 || x >= width || y < 0 || y >= height {
				continue
			}
			cells[y*width+x].Fg ^= termbox.AttrReverse
		}
	}
	termbox.Flush()
	time.Sleep(100 * time.Millisecond)
}

func (w *world) flashMessage() {
	clearMessage(termbox.ColorWhite, termbox.ColorWhite)
	termbox.Flush()
	time.Sleep(100 * time.Millisecond)
}

func (w *world) display() {
	view := w.sage.loc.add(point{-viewWidth / 2, -viewHeight / 2})

	_within := func(p point) bool {
		return p.x >= view.x && p.x < view.x+viewWidth &&
			p.y >= view.y && p.y < view.y+viewHeight
	}

	for loc, n := range w.lit {
		if !_within(loc) {
			continue
		}
		if n <= 0 {
			continue
		}
		fg := termbox.ColorDefault | termbox.AttrReverse
		bg := termbox.ColorDefault
		termbox.SetCell(loc.x-view.x, loc.y-view.y, ' ', fg, bg)
	}

	for loc, s := range w.structures {
		if !_within(loc) {
			continue
		}
		fg := termbox.ColorDefault
		if s.active() {
			fg |= termbox.AttrReverse
		}
		bg := termbox.ColorDefault
		symbol := s.def().symbol
		termbox.SetCell(loc.x-view.x, loc.y-view.y, symbol, fg, bg)
	}

	for loc, c := range w.crystalSpots {
		if loc.add(w.sage.loc.neg()).mag2() >= w.sage.sight*w.sage.sight {
			continue
		}
		if !_within(loc) {
			continue
		}
		if c.crystals <= 0 {
			continue
		}
		if w.isLit(loc) {
			continue
		}
		fg := termbox.ColorDefault
		bg := termbox.ColorDefault
		termbox.SetCell(loc.x-view.x, loc.y-view.y, '*', fg, bg)
	}

	fg := termbox.ColorDefault
	if w.isLit(w.sage.loc) {
		fg |= termbox.AttrReverse
	} else if w.sage.glowsuit > 0 {
		fg |= termbox.AttrReverse
	}
	termbox.SetCell(viewCenterX, viewCenterY, '@', fg, termbox.ColorDefault)

	for _, g := range w.grues.onMap {
		if !_within(g.loc) {
			continue
		}
		termbox.SetCell(g.loc.x-view.x, g.loc.y-view.y, ' ', termbox.ColorDefault, termbox.ColorDefault)
	}

	if w.grues.urgrue != nil {
		termbox.SetCell(w.grues.urgrue.loc.x-view.x, w.grues.urgrue.loc.y-view.y, ' ', termbox.ColorDefault, termbox.ColorDefault)
		termbox.SetCell(w.grues.urgrue.loc.x-view.x, w.grues.urgrue.loc.y-view.y-1, ' ', termbox.ColorDefault, termbox.ColorDefault)
		termbox.SetCell(w.grues.urgrue.loc.x-view.x, w.grues.urgrue.loc.y-view.y+1, ' ', termbox.ColorDefault, termbox.ColorDefault)
		termbox.SetCell(w.grues.urgrue.loc.x-view.x-1, w.grues.urgrue.loc.y-view.y, ' ', termbox.ColorDefault, termbox.ColorDefault)
		termbox.SetCell(w.grues.urgrue.loc.x-view.x+1, w.grues.urgrue.loc.y-view.y, ' ', termbox.ColorDefault, termbox.ColorDefault)
		termbox.SetCell(-1, -1, ' ', termbox.ColorDefault, termbox.ColorDefault)
	}

	for msg, count := range w.messaging.messages {
		if count == 1 {
			w.addMessage("%v", strings.Split(msg, ":")[1])
		} else {
			w.addMessage("%v (x%d)", strings.Split(msg, ":")[1], count)
		}
	}
	w.messaging.clear()
	if !w.messageAdded {
		w.messages = append(w.messages, "...")
	}
	for i := len(w.messages) - 1; i >= 0; i-- {
		delta := (len(w.messages) - 1) - i
		if delta >= messageHeight {
			break
		}
		y := messageY + messageHeight - 1 - delta
		dimPrintf(messageX, y, squish(w.messages[i], messageWidth))
	}
	if !w.messageAdded {
		w.messages = w.messages[:len(w.messages)-1]
	}
	w.messageAdded = false

	dimPrintf(statusX, statusY, "Dimlit")
	dimPrintf(statusX, statusY+1, "Crystals: %d / %d", w.sage.crystals, sageMaxCrystals)
	dimPrintf(statusX, statusY+2, "Location: %v", w.sage.loc)
	gsp := float64(w.sage.glowsuit) / float64(sageGlowsuitMaxPower) * 100
	if gsp <= sageGlowsuitWarn {
		dimPrintf(statusX, statusY+3, "Glowsuit: [%2.0f%%]", gsp)
	} else {
		dimPrintf(statusX, statusY+3, "Glowsuit: %2.0f%%", gsp)
	}
	//dimPrintf(statusX, statusY+7, "Turn: %d", w.turnCount)
	//dimPrintf(statusX, statusY+8, "Crystals on map: %d", len(w.crystalSpots))
	//dimPrintf(statusX, statusY+9, "Structures on map: %d", len(w.structures))
	//dimPrintf(statusX, statusY+10, "Light on map: %d", w.lightCount())
	//dimPrintf(statusX, statusY+11, "Grues on map: %d", len(w.grues.onMap))
	//dimPrintf(statusX, statusY+12, "Grues off map: %d", len(w.grues.offMap))
	//dimPrintf(statusX, statusY+13, "Max grues: %d", calcGrueMaxPopulation(w.turnCount))
}

func (w *world) performStructureAction(action structureAction) {
	s := w.structures[action.loc]

	switch action.kind {
	case structureRepair:
		w.sage.crystals -= s.def().repairCost
		s.damage(w, false)
		w.e.Emit(fmt.Sprintf("message:%v repaired.", s), nil)
	case structureChargeGlowsuit:
		w.sage.glowsuit = sageGlowsuitMaxPower
		w.sage.glowsuitCooldown = sageGlowsuitCooldown
		w.e.Emit("message:Charged glowsuit!", nil)
	case structureLink:
		if s.slotLinked(action.arg) {
			s.behavior.unlink(w, s, action.arg)
		}
		s.links[action.arg] = action.arg2
		s.behavior.link(w, s, action.arg2)
	case structureDeconstruct:
		w.sage.crystals = min(sageMaxCrystals, w.sage.crystals+s.def().cost/2)
		w.destroyStructureAt(action.loc)
		w.e.Emit(fmt.Sprintf("message:%v deconstructed.", s), nil)
	case structureCustomAction:
		s.behavior.doAction(w, s, action.arg)
	}
}

func (w *world) performSageAction(action sageAction) {
	switch action.kind {
	case sageBuildStructure:
		def := structureDefinitions[action.arg]
		if def.lightNeeded > w.lightLevel(action.loc) {
			w.e.Emit(fmt.Sprintf("message:%s cannot be built in darkness.", def.longName), nil)
			return
		}
		cost := def.cost
		w.sage.crystals -= cost
		s := w.addStructure(structureKind(action.arg), action.loc)
		if s != nil {
			w.e.Emit(fmt.Sprintf("message:Built %s.", def.longName), nil)
		} else {
			w.e.Emit(fmt.Sprintf("message:Could not build %s. Blocked.", def.longName), nil)
		}
	case sageToggleRunning:
		w.sage.running = !w.sage.running
		if !w.sage.running {
			w.sage.speed = 1
		}
	case sageGiveCrystals:
		w.sage.crystals += action.arg
	}
}

func (w *world) addMessage(format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	w.messages = append(w.messages, s)
	w.messageAdded = true
}

func (w *world) destroyStructureAt(loc point) {
	s := w.structures[loc]
	if s == nil {
		return
	}
	s.hamper(w, true)
	delete(w.structures, loc)
}

func (w *world) addStructure(kind structureKind, loc point) *structure {
	_, exists := w.structures[loc]
	if exists {
		return nil
	}

	s := newStructure(w.nextID, kind, loc)
	w.nextID++
	w.structures[loc] = s
	return s
}

func (w *world) findStructureByID(id int) *structure {
	for _, s := range w.structures {
		if s.id == id {
			return s
		}
	}
	return nil
}

func (w *world) structureAt(loc point) *structure {
	s, _ := w.structures[loc]
	return s
}

func (w *world) sortedStructuresByDistance(loc point) []*structure {
	structs := w.structureList()
	sort.Slice(structs, func(i, j int) bool {
		iDist := structs[i].loc.distanceTo(loc)
		jDist := structs[j].loc.distanceTo(loc)
		return iDist < jDist
	})
	return structs
}

func (w *world) structureList() []*structure {
	structs := []*structure{}
	for _, s := range w.structures {
		structs = append(structs, s)
	}
	return structs
}

func (w *world) damageSage(damage int) {
	dmg := min(damage, w.sage.glowsuit)
	w.sage.glowsuit = max(0, w.sage.glowsuit-damage)
	if dmg >= 0 {
		w.e.Emit(fmt.Sprintf("message:You took damage. Your glowsuit lost %d power!", dmg), nil)
	}
}

func (w *world) bumpSage(damage int) {
	if w.sage.bumpedThisTurn {
		return
	}
	w.sage.bumpedThisTurn = true
	w.shoveSage(randomDirection8(w.rng))
	w.sage.glowsuit = max(0, w.sage.glowsuit-damage)
	if w.sage.glowsuit == 0 {
		w.e.Emit("message:You've been eaten.", nil)
		w.sage.dead = true
		return
	}
	w.e.Emit(fmt.Sprintf("message:Something bumped into you! Your glowsuit lost %d power!", damage), nil)
}

func (w *world) bumpSageInDir(damage int, dir point) {
	if w.sage.bumpedThisTurn {
		return
	}
	w.sage.bumpedThisTurn = true
	w.shoveSage(dir.randomWeighted(w.rng, dirWeightTable{10, 5, 0, 0, 0}))
	w.sage.glowsuit = max(0, w.sage.glowsuit-damage)
	if w.sage.glowsuit == 0 {
		w.e.Emit("message:You've been eaten.", nil)
		w.sage.dead = true
		return
	}
	w.e.Emit(fmt.Sprintf("message:Something bumped into you! Your glowsuit lost %d power!", damage), nil)
}

func (w *world) teleportSage(loc point) {
	i := 1
	for {
		x, y := spiral(i)
		test := loc.add(point{x, y})
		_, sExists := w.structures[test]
		if !sExists {
			w.sage.loc = test
			w.moveSage(point{0, 0})
			return
		}
		i++
	}
}

func (w *world) shoveSage(delta point) {
	to := w.sage.loc.add(delta)
	_, sExists := w.structures[to]
	if sExists {
		return
	}

	if !w.isLit(to) {
		c, exists := w.crystalSpots[to]
		if exists && c.crystals > 0 {
			needs := sageMaxCrystals - w.sage.crystals
			if needs >= c.crystals {
				w.sage.crystals += c.crystals
				w.e.Emit(fmt.Sprintf("message:You picked up %v crystals.", c.crystals), nil)
				c.crystals = 0
			} else {
				w.e.Emit("message:You picked up some of the crystals. Your bag is full.", nil)
				w.sage.crystals += needs
				c.crystals -= needs
			}
		}
	}
	if w.sage.running {
		w.sage.running = false
		w.sage.speed = 1
	}
	w.sage.loc = to
}

func (w *world) moveSage(delta point) (res moveResult) {
	if delta == (point{0, 0}) {
		w.sage.running = false
		w.sage.speed = 1
	} else if delta != w.sage.runningDir && w.sage.running {
		w.sage.runningDir = delta
		w.sage.speed = 1
	} else if w.sage.running {
		w.sage.speed = min(w.sage.speed+1, sageMaxRunSpeed)
	}
	ps := w.sage.loc.lineTo(w.sage.loc.add(delta.times(w.sage.speed)))
	if len(ps) > 1 {
		ps = ps[1:]
	}
	runCost := -1
	for _, to := range ps {
		_, sExists := w.structures[to]
		if sExists {
			res.kind = moveBumpStructure
			res.loc = to
			if w.sage.running {
				w.sage.speed = 1
			}
			return
		}

		if !w.isLit(to) {
			c, exists := w.crystalSpots[to]
			if exists && c.crystals > 0 {
				needs := sageMaxCrystals - w.sage.crystals
				if needs >= c.crystals {
					w.sage.crystals += c.crystals
					w.e.Emit(fmt.Sprintf("message:You picked up %v crystals.", c.crystals), nil)
					c.crystals = 0
				} else {
					w.e.Emit("message:You picked up some of the crystals. Your bag is full.", nil)
					w.sage.crystals += needs
					c.crystals -= needs
				}
			}
			if w.sage.running {
				runCost += sageRunningCostPerSpeed
				w.sage.glowsuit = max(0, w.sage.glowsuit-runCost)
			}
		}
		w.sage.loc = to
		res.loc = to
	}
	res.kind = moveSuccess
	return
}

type moveKind int

const (
	moveNone moveKind = iota
	moveSuccess
	moveBumpStructure
	moveDoAction
)

type moveResult struct {
	kind moveKind
	loc  point
}

func buildStructureDialogs(w *world, at point) dialog.Dialog {
	s := w.structures[at]

	status := "[Active]"
	if !s.active() {
		status = "(Inactive)"
	}

	d := dialog.New()
	d.AddItem(fmt.Sprintf("%s — #%d", s.def().longName, s.id), false, nil, nil)
	d.AddItem(fmt.Sprintf("%s", status), false, nil, nil)
	if s.power < s.def().powerNeeded {
		d.AddItem(fmt.Sprintf("Power: %d / %d (needs more power)", s.power, s.def().powerNeeded), false, nil, nil)
	} else if s.def().powerNeeded > 0 {
		d.AddItem(fmt.Sprintf("Power: [%d / %d]", s.power, s.def().powerNeeded), false, nil, nil)
	}
	if s.broken || s.hampered {
		strs := []string{}
		if s.hampered {
			strs = append(strs, "[Hampered]")
		}
		if s.broken {
			strs = append(strs, "[Broken]")
		}
		joined := strings.Join(strs, " ")
		d.AddItem(joined, false, nil, nil)
	}
	d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)

	info := s.behavior.infoList(w, s)
	if len(info) > 0 {
		for i := 0; i < len(info); i++ {
			c := info[i]
			if !s.active() {
				c = "Suppressed (not active)"
			}
			d.AddItem(c, false, nil, nil)
		}
		d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
	}

	custom := s.behavior.actionList(s)
	if len(custom) > 0 {
		for i := 0; i < len(custom); i++ {
			c := custom[i]
			if !s.active() {
				c = fmt.Sprintf("-%s-", c)
			}
			d.AddItem(c, s.active(), structureAction{structureCustomAction, at, i, 0}, nil)
		}
		d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
	}

	if s.broken {
		if w.sage.crystals >= s.def().repairCost {
			d.AddItem(fmt.Sprintf("Repair (%v crystal)", s.def().repairCost), true, structureAction{structureRepair, at, 0, 0}, nil)
		} else {
			d.AddItem(fmt.Sprintf("-Repair (%v crystal)-", s.def().repairCost), false, nil, nil)
		}
	}

	if s.def().canChargeGlowsuit {
		if w.sage.glowsuitCooldown > 0 {
			d.AddItem(fmt.Sprintf("-Charging on cooldown (%d turns)-", w.sage.glowsuitCooldown), false, nil, nil)
		} else if !s.active() {
			d.AddItem("-Charge glowsuit (inactive)-", false, nil, nil)
		} else {
			d.AddItem("Charge glowsuit", true, structureAction{structureChargeGlowsuit, at, 0, 0}, nil)
		}
	}

	infoBuild := buildStructureInfoSubdialog(w, s.kind)
	infoBuild.AddItem("Exit", true, nil, nil)
	d.AddItem("Info", true, nil, &infoBuild)

	if s.def().numLinks > 0 {
		linkDialog := dialog.New()
		linkDialog.AddItem("Link slots", false, nil, nil)
		linkDialog.AddItem(fmt.Sprintf("Range: %d", s.def().covers), false, nil, nil)
		linkDialog.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
		for i := 0; i < s.def().numLinks; i++ {
			subLink := dialog.New()
			linkedTo := "(unlinked)"
			if s.slotLinked(i) {
				s2 := w.findStructureByID(s.slot(i))
				linkedTo = fmt.Sprintf("(linked to %v)", s2)
			}
			title := fmt.Sprintf("'%s' link %s", s.def().linkNames[i], linkedTo)
			subLink.AddItem(title, false, nil, nil)
			subLink.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
			if s.slotLinked(i) {
				subLink.AddItem("Unlink", true, structureAction{structureLink, at, i, -1}, nil)
			}
			canLinkList := []*structure{}
			for _, s2 := range w.structures {
				if !s.canLinkWith(s2) {
					continue
				}
				//subLink.AddItem(fmt.Sprintf("Link to %v", s2),
				//true, structureAction{structureLink, at, i, s2.id}, nil)
				canLinkList = append(canLinkList, s2)
			}
			sort.Slice(canLinkList, func(i, j int) bool {
				return canLinkList[i].id < canLinkList[j].id
			})
			if len(canLinkList) == 0 {
				subLink.AddItem("-No linkable structures in range-", false, nil, nil)
			} else {
				buildLinkDialogRecur(w, s, title, i, subLink, canLinkList)
			}
			subLink.AddItem("Cancel", true, nil, nil)
			linkDialog.AddItem(fmt.Sprintf("'%s' link %s", s.def().linkNames[i], linkedTo), true, nil, subLink)
		}
		linkDialog.AddItem("Cancel", true, nil, nil)
		d.AddItem("Link to Structure", true, nil, linkDialog)
	}

	if !s.def().ancient {
		d.AddItem(fmt.Sprintf("Deconstruct for %v crystals", s.def().cost/2), true, structureAction{structureDeconstruct, at, 0, 0}, nil)
	}
	d.AddItem("Cancel", true, nil, nil)

	d.Activate()

	return *d
}

func buildLinkDialogRecur(w *world, s *structure, title string, slotID int, d *dialog.Dialog, structSlice []*structure) {
	if len(structSlice) > 10 {
		step := 1
		for len(structSlice)/step > 10 {
			step *= 10
		}
		for i := 0; i < len(structSlice)/step+1; i++ {
			newLow := step * i
			newHigh := min(len(structSlice), step*(i+1))
			if newLow == newHigh {
				continue
			}
			subSlice := structSlice[newLow:newHigh]
			subDialog := dialog.New()
			subDialog.AddItem(title, false, nil, nil)
			subDialog.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
			buildLinkDialogRecur(w, s, title, slotID, subDialog, subSlice)
			lowID := structSlice[newLow].id
			highID := structSlice[newHigh-1].id
			subDialog.AddItem("Cancel", true, nil, nil)
			d.AddItem(fmt.Sprintf("Link to #%d-#%d", lowID, highID), true, nil, subDialog)
		}
		return
	}
	for _, s2 := range structSlice {
		d.AddItem(fmt.Sprintf("Link to %v", s2),
			true, structureAction{structureLink, s.loc, slotID, s2.id}, nil)
	}
}

func buildStructureInfoSubdialog(w *world, skind structureKind) dialog.Dialog {
	def := structureDefinitions[skind]
	infoBuild := dialog.New()
	infoBuild.AddItem(fmt.Sprintf("%s", def.longName), false, nil, nil)
	infoBuild.AddItem(fmt.Sprintf("Your crystals: %d / %d", w.sage.crystals, sageMaxCrystals), false, nil, nil)
	infoBuild.AddItem(fmt.Sprintf("Structure cost: %v crystals", def.cost), false, nil, nil)
	infoBuild.AddItem(fmt.Sprintf("Repair cost: %v crystals", def.repairCost), false, nil, nil)
	infoBuild.AddItem(fmt.Sprintf("Power needed: %d", def.powerNeeded), false, nil, nil)
	if def.lightNeeded > 0 {
		infoBuild.AddItem("Requires light: true", false, nil, nil)
	} else {
		infoBuild.AddItem("Requires light: false", false, nil, nil)
	}
	if def.numLinks > 0 {
		infoBuild.AddItem("Links: true", false, nil, nil)
	}
	if def.covers > 0 {
		infoBuild.AddItem(fmt.Sprintf("Range: %v", def.covers), false, nil, nil)
	}
	infoBuild.AddItem("", false, nil, nil)
	infos := strings.Split(wordWrap(structureInfo[skind], statusWidth), "\n")
	for _, info := range infos {
		infoBuild.AddItem(fmt.Sprintf("%s", info), false, nil, nil)
	}
	infoBuild.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
	return *infoBuild
}

func buildSageDialogs(w *world, at point) dialog.Dialog {
	/*d := dialog.New()
	d.AddItem("Perform an action", false, nil, nil)
	d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)*/

	build := dialog.New()
	build.AddItem("Build a structure", false, nil, nil)
	build.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)

	canBuild := false
	for i, cat := range structureCategories {
		kind := structureCategoryKind(i)
		if w.sage.unlocks[kind] == 0 {
			continue
		}
		canBuild = true

		light := dialog.New()
		light.AddItem(fmt.Sprintf("%v structures", kind), false, nil, nil)
		light.AddItem(fmt.Sprintf("Your crystals: %d / %d", w.sage.crystals, sageMaxCrystals), false, nil, nil)
		light.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
		for _, i := range cat.structures {
			def := structureDefinitions[i]
			if w.sage.crystals >= def.cost {
				if def.lightNeeded > w.lightLevel(at) {
					light.AddItem(fmt.Sprintf("-%s (not enough light)-", def.shortName), false, nil, nil)
					continue
				}
				infoBuild := buildStructureInfoSubdialog(w, i)
				infoBuild.AddItem("Build", true, sageAction{sageBuildStructure, at, int(i)}, nil)
				infoBuild.AddItem("Cancel", true, nil, nil)
				light.AddItem(fmt.Sprintf("%s (%v crystals)", def.shortName, def.cost),
					true, nil, &infoBuild)
			} else {
				light.AddItem(fmt.Sprintf("-%s (%v crystals)-", def.shortName, def.cost), false, nil, nil)

			}
		}
		light.AddItem("Cancel", true, nil, nil)
		build.AddItem(fmt.Sprintf("%v structures", kind), true, nil, light)
	}
	if !canBuild {
		build.AddItem("-No buildable structures. Bug-", false, nil, nil)
	}
	build.AddItem("Cancel", true, nil, nil)
	build.Activate()
	/*d.AddItem("Build a structure", true, nil, build)

	if w.sage.running {
		d.AddItem("Stop running", true, sageAction{sageToggleRunning, at, 0}, nil)
	} else {
		d.AddItem("Run!", true, sageAction{sageToggleRunning, at, 0}, nil)
	}
	//d.AddItem("Give me 100 crystals", true, sageAction{sageGiveCrystals, at, 10000}, nil)
	d.AddItem("Cancel", true, nil, nil)
	d.Activate()*/

	return *build
}

func buildQuitDialog() dialog.Dialog {
	d := dialog.New()
	d.AddItem("Quit?", false, nil, nil)
	d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
	d.AddItem("No", true, quitNo, nil)
	d.AddItem("Yes", true, quitYes, nil)
	d.Activate()
	return *d
}

func performDebugAction(w *world, action string) {
	switch action {
	case "structures":
		for i := structureCategoryKind(0); i < structureCategoryCount; i++ {
			w.sage.unlocks[i] = 1
		}
		w.e.Emit("message:Unlocked all structures!", nil)
	case "crystals":
		w.sage.crystals = sageMaxCrystals
		w.e.Emit("message:Crystals!!", nil)
	case "timetravel":
		w.turnCount += 5000
	case "urgrue":
		w.grues.urgrue = newGrue(grueUr)
	case "darkstorm":
		w.darkness.darknessEvents = append(w.darkness.darknessEvents,
			darknessEventDefinitions[darknessEventStorm].factory(w.rng))
	case "darklightning":
		w.darkness.darknessEvents = append(w.darkness.darknessEvents,
			darknessEventDefinitions[darknessEventLightning].factory(w.rng))
	case "darktower":
		w.darkness.darknessEvents = append(w.darkness.darknessEvents,
			darknessEventDefinitions[darknessEventTower].factory(w.rng))
	}
}

func buildDebugDialog(w *world) dialog.Dialog {
	d := dialog.New()
	d.AddItem("Debug!", false, nil, nil)
	d.AddItem(fmt.Sprintf("Turn count: %d", w.turnCount), false, nil, nil)
	d.AddItem(fmt.Sprintf("Light level: %d", w.lightLevel(w.sage.loc)), false, nil, nil)
	d.AddItem(strings.Repeat("-", statusWidth), false, nil, nil)
	d.AddItem("Unlock all structures", true, "structures", nil)
	d.AddItem("Max crystals", true, "crystals", nil)
	d.AddItem("Add 5000 turns", true, "timetravel", nil)
	d.AddItem("Ur-grue", true, "urgrue", nil)
	d.AddItem("Dark storm", true, "darkstorm", nil)
	d.AddItem("Dark lightning", true, "darklightning", nil)
	d.AddItem("Dark tower", true, "darktower", nil)
	d.Activate()
	return *d
}

func buildMenuDialog() dialog.Dialog {
	d := dialog.New()
	d.AddItem("", false, nil, nil)
	d.AddItem("", false, nil, nil)
	d.AddItem("", false, nil, nil)
	d.AddItem("", false, nil, nil)
	diff := dialog.New()
	diff.AddItem("New Game", false, nil, nil)
	diff.AddItem(strings.Repeat("-", 20), false, nil, nil)
	diff.AddItem("Quasar (easy)", true, menuNewQuasarGame, nil)
	diff.AddItem("Sun (medium)", true, menuNewSunGame, nil)
	diff.AddItem("Black Hole (hard)", true, menuNewBlackHoleGame, nil)
	d.AddItem("New Game", true, nil, diff)
	d.AddItem("Quit", true, menuQuit, nil)
	d.Activate()
	return *d
}

func displayWorldDialog(w *world, d *dialog.Dialog) {
	y := statusY
	for i := 0; i < d.Length(); i++ {
		text, sel := d.Item(i)
		if sel {
			text = fmt.Sprintf("[%s]", text)
		}
		dimPrintf(statusX, y, squish(text, statusWidth))
		if d.ItemHasSubdialog(i) {
			brack := ">"
			if sel {
				brack = "[>]"
			}
			dimPrintf(statusX+statusWidth-1, y, brack)
		}
		// TODO(patraw): generalize
		ret := d.ItemReturns(i)
		if sel && ret != nil {
			a, ok := ret.(structureAction)
			if ok && a.kind == structureLink {
				s := w.structureAt(a.loc)
				s2 := w.findStructureByID(a.arg2)
				if s != nil && s2 != nil {
					view := w.sage.loc.add(point{-viewWidth / 2, -viewHeight / 2})
					locA := s.loc.add(view.neg())
					locB := s2.loc.add(view.neg())
					drawReverseLine(locA, locB, point{viewX, viewY}, point{viewWidth, viewHeight})
				}
			}
		}
		y++
	}
}

func displayDialog(d *dialog.Dialog) {
	y := statusY
	for i := 0; i < d.Length(); i++ {
		text, sel := d.Item(i)
		if sel {
			text = fmt.Sprintf("[%s]", text)
		}
		dimPrintf(statusX+20, y, squish(text, statusWidth-20))
		if d.ItemHasSubdialog(i) {
			brack := ">"
			if sel {
				brack = "[>]"
			}
			dimPrintf(statusX+statusWidth-1, y, brack)
		}
		y++
	}
}

type actionKind int

const (
	actionNone actionKind = iota
	actionLeft
	actionDown
	actionUp
	actionRight
	actionWait
	actionBack
	actionAction
	actionDebug
)

type menuAction int

const (
	menuNewQuasarGame    menuAction = iota
	menuNewSunGame       menuAction = iota
	menuNewBlackHoleGame menuAction = iota
	menuHowToPlay
	menuQuit
)

func main() {
	mainDialog := buildMenuDialog()

	termbox.Init()
	defer termbox.Close()

mainloop:
	for {
		termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
		for y, row := range titleRows {
			dimPrintf(viewX, viewY+y, "%s", row)
		}
		if mainDialog.Active() {
			displayDialog(&mainDialog)
		}
		termbox.Flush()

		if mainDialog.Active() {
			ev := termbox.PollEvent()
			action := convertKeyToActiont(&ev)
			mappings := map[actionKind]dialog.DialogAction{
				actionAction: dialog.DialogSelect,
				actionBack:   dialog.DialogLeave,
				actionUp:     dialog.DialogPrev,
				actionDown:   dialog.DialogNext,
				actionLeft:   dialog.DialogBack,
				actionRight:  dialog.DialogSelect,
				actionNone:   dialog.DialogNoAction,
			}
			dact, exists := mappings[action]
			if !exists {
				dact = dialog.DialogNoAction
			}
			res := mainDialog.PerformAction(dact)
			if a, ok := res.(menuAction); ok {
				switch a {
				case menuNewQuasarGame, menuNewSunGame, menuNewBlackHoleGame:
					gameMain(difficultyLevel(a))
					mainDialog.Activate()
				case menuQuit:
					break mainloop
				}
			}
		} else {
			break
		}
	}
}

func gameMain(diff difficultyLevel) {
	w := newWorld()
	w.diff = diff
	w.e.Emit("message:You are surrounded by darkness. Build more light.", nil)
	var mainDialog dialog.Dialog

mainloop:
	for {
		termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
		w.display()
		if mainDialog.Active() {
			clearStatus(termbox.ColorDefault, termbox.ColorDefault)
			displayWorldDialog(w, &mainDialog)
		}
		termbox.Flush()

		var result moveResult
		ev := termbox.PollEvent()
		action := convertKeyToActiont(&ev)

		if mainDialog.Active() {
			mappings := map[actionKind]dialog.DialogAction{
				actionAction: dialog.DialogSelect,
				actionBack:   dialog.DialogLeave,
				actionUp:     dialog.DialogPrev,
				actionDown:   dialog.DialogNext,
				actionLeft:   dialog.DialogBack,
				actionRight:  dialog.DialogSelect,
				actionNone:   dialog.DialogNoAction,
			}
			dact, exists := mappings[action]
			if !exists {
				dact = dialog.DialogNoAction
			}
			res := mainDialog.PerformAction(dact)
			if a, ok := res.(structureAction); ok {
				w.performStructureAction(a)
				w.update()
			}
			if a, ok := res.(sageAction); ok {
				w.performSageAction(a)
				w.update()
			}
			if a, ok := res.(quitAction); ok {
				if a == quitYes {
					break mainloop
				}
			}
			if a, ok := res.(string); ok {
				performDebugAction(w, a)
				w.update()
			}
			continue
		}

		switch action {
		case actionLeft:
			result = w.moveSage(point{-1, 0})
		case actionDown:
			result = w.moveSage(point{0, 1})
		case actionUp:
			result = w.moveSage(point{0, -1})
		case actionRight:
			result = w.moveSage(point{1, 0})
		case actionWait:
			result = w.moveSage(point{0, 0})
		case actionAction:
			result.kind = moveDoAction
		case actionBack:
			mainDialog = buildQuitDialog()
			continue mainloop
		case actionDebug:
			mainDialog = buildDebugDialog(w)
			continue mainloop
		case actionNone:
		}

		if result.kind == moveNone {
			continue
		}
		if result.kind == moveBumpStructure {
			mainDialog = buildStructureDialogs(w, result.loc)
		} else if result.kind == moveDoAction {
			mainDialog = buildSageDialogs(w, w.sage.loc)
		} else {
			w.update()
		}

		if w.sage.dead {
			w.e.Emit(fmt.Sprintf("message:You lasted %d turns.", w.turnCount), nil)
			w.update()
			termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
			w.display()
			termbox.Flush()
			for {
				ev = termbox.PollEvent()
				action := convertKeyToActiont(&ev)
				if action == actionBack {
					break mainloop
				}
			}
		}
	}

	w.e.Emit("sound:stopAll")

}

func clearRect(topLeft, bottomRight point, fg, bg termbox.Attribute) {
	for x := topLeft.x; x < bottomRight.x; x++ {
		for y := topLeft.y; y < bottomRight.y; y++ {
			termbox.SetCell(x, y, ' ', fg, bg)
		}
	}
}

func clearStatus(fg, bg termbox.Attribute) {
	a := point{statusX, statusY}
	b := point{statusX + statusWidth, statusY + statusHeight}
	clearRect(a, b, fg, bg)
}

func clearView(fg, bg termbox.Attribute) {
	a := point{viewX, viewY}
	b := point{viewX + viewWidth, viewY + viewHeight}
	clearRect(a, b, fg, bg)
}

func clearMessage(fg, bg termbox.Attribute) {
	a := point{messageX, messageY}
	b := point{messageX + messageWidth, messageY + messageHeight}
	clearRect(a, b, fg, bg)
}

func convertKeyToActiont(ev *termbox.Event) actionKind {
	switch ev.Ch {
	case 'h':
		return actionLeft
	case 'j':
		return actionDown
	case 'k':
		return actionUp
	case 'l':
		return actionRight
	case ' ':
		return actionAction
	case '.':
		return actionWait
	case 'Q':
		return actionBack
	case 'Z':
		return actionDebug
	}

	switch ev.Key {
	case termbox.KeyArrowLeft:
		return actionLeft
	case termbox.KeyArrowDown:
		return actionDown
	case termbox.KeyArrowUp:
		return actionUp
	case termbox.KeyArrowRight:
		return actionRight
	case termbox.KeySpace, termbox.KeyEnter:
		return actionAction
	case termbox.KeyEsc:
		return actionBack
	}

	return actionNone
}
