package main

import (
	"github.com/olebedev/emitter"
)

type hearing interface {
	canHear(from point) bool
}

type messagingSystem struct {
	messages map[string]int
	e        *emitter.Emitter
}

func newMessaging(e *emitter.Emitter, hear hearing) *messagingSystem {
	m := &messagingSystem{
		make(map[string]int, 0),
		e,
	}
	m.e.On("message:*", func(event *emitter.Event) {
		loc := event.Args[0]
		if loc == nil || hear.canHear(*(loc.(*point))) {
			n := m.messages[event.OriginalTopic]
			m.messages[event.OriginalTopic] = n + 1
		}
	})
	return m
}

func (m *messagingSystem) clear() {
	m.messages = make(map[string]int, 0)
}
