Dimlit is a Roguelike game played in the terminal with ASCII graphics. You play as a Sage surviving in an apocalyptic world consumed by infinite darkness populated by grues. Your only defense against them and the oppressing darkness are crystalline structures you build, some of which provide light.

YOU - THE SAGE

You are a sage. You cannot attack and you die instantly by any grue attack. You can hold off attacks in the darkness  for a short while  with your glow suit, but it can only be powered at a functioning  crystalline structure. Your glowsuit is your life in the darkness.

You start off in the center of an infinite map of darkness, with  a single crystalline structure next to you providing light. To keep yourself alive you must build more of these structures with crystals found only in the darkness.

THE CRYSTALLINE STRUCTURES

Crystalline Structures are essential to the survival of the Sage. You must build more if you wish to survive. You will experience darkness surges and grue attacks which can leave you stranded. The events and attacks cannot be prevented, you must be prepared to survive.

Some structures require power, specifically tech upgrade structures and more advanced structures. You start off with the ability to build a basic power-providing structure  aptly named "Power Structure." It provides one power to a structure it links to directly next to it (horizontally/vertically).
Power structures are not the only structures that have links. Explore different structures by unlocking them.

THE LIGHT

Light is essential. Without it your glowsuit loses and power and you will succumb to the darkness and the grues. A larger network of light gives you a safe place to run if a storm or grue takes out your nearby structures.

THE DARKNESS

The darkness is infinite, and becomes more oppressive after each turn. The grue population is continuously increasing and dark surges and storms put your protected structures at further risk. 

THE GRUES

Most grues avoid any and all light, however there are some which can withstand the light for a short while to take out structures or attack you. Grues can  and should be killed by late game structures. If they aren't, more of them attack you. 

THE GOAL

The goal is to survive for as long as possible.

CONTROLS

Sage
	Move - HJKL, arrows
	Wait - Period
	Action - Space
	Interact with Structure - Bump into it
Menus / Structure Menus
	Select - Space, L
	Back - Escape, Q, H
	Up/Down - JK
General
	Quit - Escape, Q

GAMEPLAY TIPS

	Collect crystals to build more structures.
	Build light to find more crystals deep in the darkness.
	Unlock new structures by powering tech structures.
	Hampered structures only as long as the storm does. Wait it out.
	Structures damaged by grues can be repaired.
	Lit up structures are functioning.
	You can disable sound by renaming the "data" folder to something else.

Have fun!